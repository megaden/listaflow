# Listaflow

Listaflow is an Open Source web application for tracking your recurring business
practices and checklists. If you have processes that need to be done periodically and benefit from a pre-created to-do list each time, Listaflow is for you.

Listaflow was developed by OpenCraft to solve our recurring process problems.
We (will) use it to help us organize sprint planning and vacations, among other things.

## Documentation
- [Developing Listaflow](./docs/development.md)
- [Listaflow Frontend](./docs/frontend.md)
- [Frontend Deployment With Cloudflare](./docs/deployment.md#frontend---cloudflare)
- [Backend Deployment](./docs/deployment.md#backend-deployment)
- [SSO Support](./docs/sso_support.md)
- [Internationalization](./docs/localization.md)

## UX/UI Design

To view information about the user experience (UX) and user interface (UI) design of
this project, visit the [design repository](https://gitlab.com/opencraft/dev/listaflow-design).

## How to use Listaflow?

Listaflow uses a simple concept of Teams and assigns a recurring task to the team.

### Workflow

To create a list of tasks, A [`checklist definition`](http://localhost:8000/admin/workflow/checklistdefinition/) should be created.

The Checklist definition is the place where the item on the checklist is added. The `interface type` is the type of answer i.e the kind of response that is accepted. For now, the supported types are:

- Numeric
- Checkbox
- Linear Scale Rating
- Markdown Text Area

After creating the checklist definition, this definition is utilized in [Reccurence](http://localhost:8000/admin/workflow/recurrence/), when recurrence is created it asks to be linked to a checklist definition, this is a way the checklist is scheduled in subsequent intervals.

The `Interval Schedule` field is the time duration between two checklists. The `Reminder days` field is the number of days before the due date of the checklist when a reminder email is sent to the user.
