import { render } from "../../utils/test-utils";
import { useTranslation } from "react-i18next";
import { CustomGoogleLogin } from "../CustomGoogleLogin";

const { t } = useTranslation();

jest.mock("../../constants/api-urls", () => ({
  TOKEN_PATH: "#",
}));

test("test google button hidden", async () => {
  const { queryByText } = render(<CustomGoogleLogin setErrors={() => {}} />);

  expect(queryByText(t("loginWithGoogle"))).toBeNull();
});

test("test visible google button", async () => {
  process.env.REACT_APP_GOOGLE_CLIENT_ID = "fake-id";
  const { queryByText } = render(<CustomGoogleLogin setErrors={() => {}} />);

  expect(queryByText(t("loginWithGoogle"))).toBeTruthy();
});
