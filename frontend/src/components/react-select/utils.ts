import { Options, PropsValue } from "react-select";

export const cleanValue = <Option>(
  value: PropsValue<Option>
): Options<Option> => {
  if (Array.isArray(value)) return value.filter(Boolean);
  if (typeof value === "object" && value !== null) return [value as Option];
  return [];
};
