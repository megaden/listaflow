import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { ListController } from "@opencraft/providence/base/lists/types/ListController";
import Alert from "react-bootstrap/Alert";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { ReactComponent as WarningIcon } from "../assets/icons/warning.svg";

declare interface ErrorAlertArgs {
  controllers: Array<SingleController<any> | ListController<any>>;
}

export const ErrorAlerts = ({ controllers }: ErrorAlertArgs) => {
  const errors: string[] = [];
  controllers.map((controller) => errors.push(...controller.errors.messages));
  return (
    <Row>
      {errors.map((message) => (
        <Col xs={12} key={message}>
          <Alert
            className="pb-0 bg-danger text-dark border-danger"
            variant="danger"
          >
            <p>
              <WarningIcon className="smiley-icon me-2" />
              {message}
            </p>
          </Alert>
        </Col>
      ))}
    </Row>
  );
};
