import { useList, useSingle } from "@opencraft/providence/react-plugin";
import { Run, Task, TaskList } from "../types/Task";
import { Paginated } from "../components/Paginated";
import { TaskItem } from "../components/TaskItem";
import { WORKFLOW_API } from "../constants/api-urls";
import Accordion from "react-bootstrap/Accordion";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Body } from "../components/Body";
import { Title } from "../components/Title";
import { ReactComponent as SmileyIcon } from "../assets/icons/smiley.svg";
import { ReactComponent as WarningIcon } from "../assets/icons/warning.svg";
import { LoadSection } from "../components/LoadSection";
import { ProgressState } from "../components/ChecklistProgress";
import useAuth from "../hooks/useAuth";
import { useTranslation } from "react-i18next";
import { dateString } from "../utils/helpers";
import { ConfettiState } from "../components/Confetti";

type ChecklistDetailRouteParams = {
  checklistId: string;
};

export const ChecklistDetail = () => {
  const { t } = useTranslation();
  const { auth } = useAuth();
  const username = auth?.username!;
  const { checklistId } = useParams<ChecklistDetailRouteParams>();
  const checklistUrl = `${WORKFLOW_API}/user/${username}/checklist`;

  // checklist details
  const checklistController = useSingle<TaskList>([username, checklistId!], {
    endpoint: `${checklistUrl}/${checklistId}/`,
  });

  // track submission count to update submit button text
  const submissionCount = useSingle<{ num: number }>(
    [checklistId!, "submissionCount"],
    {
      endpoint: "#",
      x: { num: 0 },
    }
  );

  useEffect(() => {
    // update checklistController on page number change to handle hidden completed tasks
    checklistController.get().then(() => {
      if (checklistController.x?.completed) {
        submissionCount.p.num.model = 1;
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // tasks list
  const tasksController = useList<Task>([checklistId!, "task"], {
    endpoint: `${checklistUrl}/${checklistId}/task/`,
    paginated: false,
  });

  useEffect(() => {
    tasksController.get();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checklistId, username]);

  // progress bar controller
  const progressValue = useSingle<ProgressState>("taskProgress", {
    endpoint: "#",
  });
  // confetti controller
  const confettiState = useSingle<ConfettiState>("confettiState", {
    endpoint: "#",
  });
  const totalTasks = tasksController.count;
  let completedTasks = 0;
  let requiredCompletedTasks = 0;
  let requiredTotalTasks = 0;
  let tasksWithInteraction: string[] = [];
  let failed = false;

  tasksController.list.forEach((task) => {
    if (task.x?.required) {
      requiredTotalTasks += 1;
      if (task.x?.completed) {
        requiredCompletedTasks += 1;
      }
      if (task.p?.response?.errors?.length) {
        failed = true;
      }
    }
    if (task.x?.completed) {
      completedTasks += 1;
    }

    if (task.x?.interface_type !== "checkbox") {
      tasksWithInteraction.push(task.x!.id);
    }
  });

  if (requiredTotalTasks) {
    progressValue.p.val.model =
      (requiredCompletedTasks / requiredTotalTasks) * 100;
  }

  const pendingTasks = requiredTotalTasks
    ? requiredTotalTasks - requiredCompletedTasks
    : -1;

  if (pendingTasks > 0) {
    if (!checklistController.p.completed.dirty) {
      checklistController.p.completed.model = false;
    }
  }
  const items_text =
    pendingTasks === 1
      ? t("checklistDetail.itemSingular")
      : t("checklistDetail.itemPlural");
  const submitChecklist = () => {
    checklistController.p.completed.errors = [];
    checklistController
      .patch({ completed: true })
      .then((x) => {
        if (x.completed) {
          submissionCount.p.num.model += 1;
          confettiState.p.fire.model = Math.random();
        }
        checklistController.setX(x);
      })
      .catch((err) => {
        checklistController.p.completed.errors = err.response.data.completed;
      });
  };

  const RunInfo = ({ run_info }: { run_info: Run }) => {
    const skipYear =
      new Date(run_info.start_date).getFullYear() ===
      new Date(run_info.end_date).getFullYear();
    const start_date = dateString(run_info?.start_date, skipYear);
    const end_date = dateString(run_info?.end_date);
    const dateRangeStr = `${start_date} - ${end_date}`;
    return (
      <div dir="auto">
        <p className="mb-0">
          <b>{t("checklistDetail.team")} </b>
          {run_info.team_name}
        </p>
        <p>
          <small>{dateRangeStr}</small>
        </p>
      </div>
    );
  };

  return (
    <LoadSection controllers={[checklistController]}>
      {() => (
        <Container>
          <Row>
            <Col dir="auto">
              <h1 className="fw-bold">
                <Title text={checklistController.x!.name} />
              </h1>
              {checklistController.x?.run && (
                <RunInfo run_info={checklistController.x?.run} />
              )}
              <Body text={checklistController.x!.body} />
            </Col>
          </Row>
          <Row>
            <small className="text-muted mb-4">
              {t("checklistDetail.form.completeOnTotal", {
                completedTasks,
                totalTasks,
              })}
            </small>
          </Row>
          <Paginated controller={tasksController}>
            <Accordion
              className="px-0 mx-3 mb-3 border-bottom border-2 border-light-navy"
              defaultActiveKey={tasksWithInteraction}
              alwaysOpen
              flush
            >
              {tasksController.list.map((singleController) => (
                <TaskItem
                  controller={singleController}
                  key={singleController.x!.id}
                />
              ))}
            </Accordion>
          </Paginated>
          {!checklistController.p.completed.errors?.length && pendingTasks > 0 && (
            <Row className="mt-5 pt-2">
              <p>
                {t("checklistDetail.form.pendingTasks", {
                  pendingTasks,
                  itemsText: items_text,
                })}
              </p>
            </Row>
          )}
          {pendingTasks === 0 &&
            !checklistController.p.completed.errors?.length &&
            checklistController.p.completed.model && (
              <Row className="mt-3">
                <Col>
                  <Alert
                    className="pb-0 bg-success text-dark"
                    variant="success"
                  >
                    <p>
                      <SmileyIcon className="smiley-icon me-2" />
                      {t("checklistDetail.form.checklistCompleted")}
                    </p>
                  </Alert>
                </Col>
              </Row>
            )}
          {checklistController.p.completed.errors?.map((error) => (
            <Row key={error} className="mt-3">
              <Col>
                <Alert
                  className="pb-0 bg-danger text-dark border-danger"
                  variant="danger"
                >
                  <p>
                    <WarningIcon className="smiley-icon me-2" />
                    {error}
                  </p>
                </Alert>
              </Col>
            </Row>
          ))}
          <Row className="mt-3">
            {(!checklistController.p.completed.model ||
              checklistController.p.completed.errors?.length !== 0) && (
              <Col md={6}>
                <Button
                  variant="primary"
                  onClick={submitChecklist}
                  disabled={failed}
                >
                  {submissionCount.p.num.model > 0
                    ? t("checklistDetail.form.submitBtnAgain")
                    : t("checklistDetail.form.submitBtn")}
                </Button>
              </Col>
            )}
          </Row>
        </Container>
      )}
    </LoadSection>
  );
};
