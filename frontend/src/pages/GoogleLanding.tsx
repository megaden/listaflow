import useAuth from "../hooks/useAuth";
import { useLocation, useNavigate } from "react-router-dom";
import {
  GoogleLoginResponse,
  GoogleLoginResponseOffline,
  useGoogleLogin,
} from "react-google-login";
import axios from "axios";

import Spinner from "react-bootstrap/Spinner";
import { HOME_PAGE, LOGIN as LOGIN_URL } from "../constants/urls";
import { googleRedirectUri } from "../utils/helpers";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { LocationState } from "../types/Common";

// a landing page for google redirects which shows a spinner until oauth token is fetched
export const GoogleLanding = () => {
  const { googleLogin } = useAuth();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const location = useLocation();
  const locationState = location.state as LocationState;

  const responseGoogle = async (
    response: GoogleLoginResponse | GoogleLoginResponseOffline
  ) => {
    try {
      if ("accessToken" in response) {
        await googleLogin(response.accessToken);
        navigate(locationState?.from?.pathname || HOME_PAGE, { replace: true });
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        navigate(LOGIN_URL, { replace: true, state: locationState });
      } else {
        throw error;
      }
    }
  };

  useGoogleLogin({
    clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID!,
    uxMode: "redirect",
    onSuccess: responseGoogle,
    onFailure: responseGoogle,
    isSignedIn: true,
    redirectUri: googleRedirectUri(),
    cookiePolicy: "single_host_origin",
  });

  useEffect(() => {
    const timer = setTimeout(() => {
      // redirect user to login in case of timeout (10 seconds)
      navigate(LOGIN_URL, { replace: true, state: locationState });
    }, 10 * 1000);
    return () => clearTimeout(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <h5 className="text-center">{t("googleLanding.waitAndNotRefresh")}</h5>
      <div className="d-flex justify-content-center">
        <Spinner animation="border" role="status">
          <span className="visually-hidden">{t("pageStatuses.loading")}</span>
        </Spinner>
      </div>
    </>
  );
};
