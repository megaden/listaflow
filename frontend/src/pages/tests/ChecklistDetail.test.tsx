import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { createStore, IModuleStore } from "redux-dynamic-modules";
import { ChecklistDetail } from "../ChecklistDetail";

let mockReadyVariable: Boolean = true;

// mock useList from providence
jest.mock("@opencraft/providence/react-plugin", () => {
  return {
    useList: () => {
      return {
        getOnce: () => {},
        get: () => {
          return { catch: () => {} };
        },
        setPage: (pageNum: number) => {
          pageNum;
        },
        errors: { messages: ["this is an error"] },
        ready: mockReadyVariable,
        list: [
          {
            x: {
              id: 1,
              label: "delectus aut autem",
              body: "body 1",
              completed: false,
              interface_type: "checkbox",
            },
            p: { response: {} },
          },
          {
            x: {
              id: 2,
              label: "quis ut nam facilis et officia qui",
              body: "body 2",
              completed: true,
              interface_type: "checkbox",
            },
            p: { response: {} },
          },
        ],
      };
    },
    useSingle: () => {
      return {
        getOnce: () => {},
        get: () => {
          return { then: () => {}, catch: () => {} };
        },
        errors: { messages: ["this is an error"] },
        ready: mockReadyVariable,
        p: { completed: {}, num: {} },
        x: {
          name: "some name",
          body: "some body",
          num: 0,
        },
      };
    },
  };
});

// mock useList from providence
jest.mock("react-router-dom", () => {
  return {
    useSearchParams: () => {
      return [{ get: () => 1 }, () => {}];
    },
    useParams: () => {
      return { checklistId: "someId" };
    },
  };
});

test("renders providence list items", () => {
  const store: IModuleStore<{}> = createStore({});
  render(
    <Provider store={store}>
      <ChecklistDetail />
    </Provider>
  );
  const checklist1 = screen.getByText(/delectus aut autem/i);
  expect(checklist1).toBeInTheDocument();

  const checklist2 = screen.getByText(/quis ut nam facilis et officia qui/i);
  expect(checklist2).toBeInTheDocument();
});

test("renders providence errors", () => {
  mockReadyVariable = false;
  const store: IModuleStore<{}> = createStore({});
  render(
    <Provider store={store}>
      <ChecklistDetail />
    </Provider>
  );

  const checklist1 = screen.getByText(/This is an error/i);
  expect(checklist1).toBeInTheDocument();
});
