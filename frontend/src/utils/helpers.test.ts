import {
  clearUserSession,
  dateString,
  downloadFile,
  getCookie,
  googleRedirectUri,
  isFirstOlderThanSecond,
  parseToDate,
  setUserSession,
} from "./helpers";
import MockAdapter from "axios-mock-adapter";
import axios from "axios";
import { flushPromises } from "./test-utils";

describe("downloadFile", () => {
  const mock = new MockAdapter(axios, { onNoMatch: "throwException" });
  beforeEach(() => {
    jest.clearAllMocks();
    mock.reset();
  });
  it("Downloads something", async () => {
    mock.onGet("https://localhost/url/to/file").reply(200, { data: "Beep" });
    downloadFile("https://localhost/url/to/file", "file.pdf");
    await flushPromises();
    expect(mock.history.get[0].url).toBe("https://localhost/url/to/file");
  });
});

describe("isFirstOlderThanSecond", () => {
  [
    {
      first: "2022-11-01T18:58:35.852525Z",
      second: "2022-11-12T18:58:35.852525Z",
      result: true,
    },
    {
      first: "2022-11-15T18:58:35.852525Z",
      second: "2022-11-12T18:58:35.852525Z",
      result: false,
    },
    {
      first: "2022-11-12T18:58:35.852525Z",
      second: "2022-11-12T18:58:35.852525Z",
      result: false,
    },
    { first: "", second: "", result: false },
    { first: "2022-11-12T18:58:35.852525Z", second: "", result: false },
    { first: "", second: "2022-11-12T18:58:35.852525Z", result: false },
  ].forEach((params) => {
    const { first, second, result } = params;
    const name =
      `Recognizes that ${first} is ` +
      ("" ? result : "not ") +
      `older than ${second}`;
    it(name, () => {
      expect(isFirstOlderThanSecond(first, second)).toBe(result);
    });
  });
});

describe("dateString", () => {
  [
    {
      input: "2022-11-01T08:58:35.852525Z",
      skipYear: false,
      output: "Nov 1, 2022",
    },
    { input: "2022-11-01T08:58:35.852525Z", skipYear: true, output: "Nov 1" },
    { input: "", skipYear: true, output: "" },
  ].forEach((params) => {
    const { input, skipYear, output } = params;
    const name = `Renders ${input} to ${output}.`;
    it(name, () => {
      expect(dateString(input, skipYear)).toBe(output);
    });
  });
});

describe("parseDate", () => {
  const date = new Date("2022-11-01T18:58:35.852525Z");
  [
    { input: "2022-11-01T18:58:35.852525Z", output: date },
    { input: date, output: date },
  ].forEach((params) => {
    const { input, output } = params;
    it(`Resolves ${input} to ${output}`, () => {
      expect(parseToDate(input)).toEqual(output);
    });
  });
});

describe("googleRedirectUri", () => {
  it("Resolves the Google redirect URI", () => {
    expect(googleRedirectUri()).toBe("http://localhost/google-landing");
  });
});

describe("clearUserSession", () => {
  beforeEach(() => {
    localStorage.setItem("access_token", "1234");
    localStorage.setItem("refresh_token", "5678");
    localStorage.setItem("id", "123");
    localStorage.setItem("username", "Boop");
  });
  it("Clears a user's session", () => {
    // Sanity check
    clearUserSession();
    expect(localStorage.getItem("access_token")).toBeNull();
    expect(localStorage.getItem("refresh_token")).toBeNull();
    expect(localStorage.getItem("id")).toBeNull();
    expect(localStorage.getItem("username")).toBeNull();
  });
  it("Clears a user's session while leaving their username intact", () => {
    clearUserSession(true);
    expect(localStorage.getItem("access_token")).toBeNull();
    expect(localStorage.getItem("refresh_token")).toBeNull();
    expect(localStorage.getItem("id")).toBeNull();
    expect(localStorage.getItem("username")).toBe("Boop");
  });
});

describe("setUserSession", () => {
  beforeEach(() => {
    localStorage.setItem("access_token", "default");
    localStorage.setItem("refresh_token", "default");
    localStorage.setItem("id", "default");
    localStorage.setItem("username", "default");
  });
  afterEach(() => {
    localStorage.clear();
  });
  it("Sets all values", () => {
    setUserSession({
      access_token: "1234",
      refresh_token: "5678",
      username: "Beep",
      id: 123,
    });
    expect(localStorage.getItem("access_token")).toEqual("1234");
    expect(localStorage.getItem("refresh_token")).toEqual("5678");
    expect(localStorage.getItem("id")).toEqual("123");
    expect(localStorage.getItem("username")).toEqual("Beep");
  });
  it("Sets no values", () => {
    setUserSession({
      access_token: "",
      refresh_token: "",
      username: "",
      id: 0,
    });
    expect(localStorage.getItem("access_token")).toEqual("default");
    expect(localStorage.getItem("refresh_token")).toEqual("default");
    expect(localStorage.getItem("id")).toEqual("default");
    expect(localStorage.getItem("username")).toEqual("default");
  });
  it("Sets some values", () => {
    setUserSession({
      access_token: "1234",
      refresh_token: "",
      username: "Beep",
      id: 0,
    });
    expect(localStorage.getItem("access_token")).toEqual("1234");
    expect(localStorage.getItem("refresh_token")).toEqual("default");
    expect(localStorage.getItem("id")).toEqual("default");
    expect(localStorage.getItem("username")).toEqual("Beep");
  });
});

describe("getCookie", () => {
  beforeEach(() => {
    Object.defineProperty(document, "cookie", {
      writable: true,
      value: "csrftoken=beepboop; thing=foobar; test=foo%26bar",
    });
  });
  it("Gets a cookie value", () => {
    expect(getCookie("csrftoken")).toBe("beepboop");
  });
  it("Gets different cookie values", () => {
    expect(getCookie("csrftoken")).toBe("beepboop");
    expect(getCookie("thing")).toBe("foobar");
  });
  it("Decodes escaped values", () => {
    expect(getCookie("test")).toEqual("foo&bar");
  });
});
