"""
profile related models
"""
from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.db.models import CharField, EmailField
from django.utils.translation import gettext_lazy as _

from short_stuff import gen_shortcode
from short_stuff.django.models import ShortCodeField
from taggit.managers import TaggableManager

from lib.models import TaggedItemBase


class CustomUserManager(UserManager):
    """
    Custom user model manager
    """

    def get_by_natural_key(self, username):
        """
        make username matches case-insensitive
        """
        case_insensitive_username_field = f"{self.model.USERNAME_FIELD}__iexact"
        return self.get(**{case_insensitive_username_field: username})


class User(AbstractUser):
    """
    Custom user for listaflow with unique email field and single name field
    """

    username_validator = UnicodeUsernameValidator()
    display_name = CharField(_("Name of User"), blank=True, null=True, max_length=255)
    username = CharField(
        _("username"),
        max_length=150,
        unique=True,
        db_index=True,
        help_text=_(
            "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
        ),
        validators=[username_validator],
        error_messages={
            "unique": _("A user with this username already exists."),
        },
    )
    email = EmailField(
        _("email address"),
        unique=True,
        error_messages={
            "unique": _("A user with this email already exists."),
        },
    )

    objects = CustomUserManager()

    REQUIRED_FIELDS = ["username"]
    USERNAME_FIELD = "email"

    def update_display_name(self):
        """
        create display_name using first name and last name if present else
        username
        """
        if self.first_name or self.last_name:
            self.display_name = str(self.first_name + " " + self.last_name).strip()
        else:
            self.display_name = self.username

    def clean(self) -> None:
        super().clean()
        self.username = self.username.lower()
        if not self.display_name:
            self.update_display_name()

    def save(self, *args, **kwargs) -> None:
        self.clean()
        return super().save(*args, **kwargs)

    def get_full_name(self) -> str:
        """
        returns user full name
        """
        return self.display_name

    def get_short_name(self) -> str:
        """
        returns user short name
        """
        return self.display_name

    @property
    def teams(self) -> list:
        """
        Get all teams
        """
        return self.teams.all()


class TaggedMembership(TaggedItemBase):
    """
    Custom tagged through model, in order to reduce lookup count in
    already-dense queries. Otherwise, it would use a GenericForeignKey.
    """

    content_object = models.ForeignKey("TeamMembership", on_delete=models.CASCADE)


class TeamMembership(models.Model):
    """Through model for Team<->User many-to-many relationship."""

    team = models.ForeignKey("Team", on_delete=models.CASCADE)
    user = models.ForeignKey("User", on_delete=models.CASCADE)
    tags = TaggableManager(through=TaggedMembership, blank=True)


class Team(models.Model):
    """The Team model for creating a team containing multiple users."""

    id = ShortCodeField(primary_key=True, db_index=True, default=gen_shortcode)
    name = models.CharField(max_length=64, unique=True)
    description = models.TextField(max_length=1024)
    members = models.ManyToManyField(
        "User", through=TeamMembership, related_name="teams"
    )

    def __str__(self):
        return f"{self.name} [{self.id}]"
