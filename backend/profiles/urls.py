"""
Listaflow profile URL Configuration
"""
from django.urls import path
from django.urls.conf import include

from rest_framework_nested import routers

from profiles.views import (
    resend_verification_email,
    verify_registration,
    UserTeamViewSet,
    UserTeamNamesViewSet,
)


user_info_router = routers.SimpleRouter(r"")
app_name = "profiles"
urlpatterns = [
    path("accounts/", include("rest_registration.api.urls")),
    path("accounts/teams/", UserTeamViewSet.as_view({"get": "list"}), name="teams"),
    path(
        "accounts/team-names/",
        UserTeamNamesViewSet.as_view({"get": "list"}),
        name="user-teams",
    ),
    path(
        "resend_verification_email/",
        resend_verification_email,
        name="resend-verification-email",
    ),
    path("verify-registration/", verify_registration, name="verify-registration"),
]
