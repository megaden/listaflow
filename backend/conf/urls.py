"""Listaflow URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

import views

# add urls here to view them in swagger docs
public_api_patterns = [
    path("api/workflow/", include("workflow.urls")),
    path("api/profile/", include("profiles.urls")),
    path("auth/", include("drf_social_oauth2.urls", namespace="drf")),
]

ApiDocs = get_schema_view(
    openapi.Info(
        title="Listaflow API",
        default_version="v1",
        description="This is Listaflow's API",
        # This should probably be made overwritable eventually.
        terms_of_service="https://www.example.com/terms-of-service",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="AGPLv3"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
    patterns=public_api_patterns,
)

urlpatterns = public_api_patterns + [
    # These URL names are apparently the default/standard, so while they're a bit odd,
    # we should probably keep them to encourage easy interoperability.
    path("api/", views.bad_endpoint, name="api404"),
    path(
        "swagger.yaml",
        ApiDocs.without_ui(cache_timeout=0),
        kwargs={"format": "json"},
        name="schema-yaml",
    ),
    path(
        "swagger.json",
        ApiDocs.without_ui(cache_timeout=0),
        kwargs={"format": "json"},
        name="schema-json",
    ),
    path(
        "swagger/",
        ApiDocs.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("admin/", admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += [
        re_path(".*", views.index, name="index"),
    ]
