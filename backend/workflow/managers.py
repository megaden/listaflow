"""Django model managers"""

from django.db import models
from django.db.models import F, Q
from django.utils import timezone

from profiles.models import Team, User


class ChecklistQuerySet(models.query.QuerySet):
    """Customized QuerySet for Checklist"""

    @classmethod
    def status_to_do_query(cls):
        """
        Query for filtering to do checklist
        """
        today = timezone.now().date()
        not_completed = Q(completed=False)
        no_recurrence = Q(run__isnull=True)
        has_recurrence = Q(run__isnull=False)
        end_date_lt_due_gt_now = (
            Q(
                due_date__gte=today,
                end_date__date__lte=F("due_date"),
            )
            & Q(end_date__date__lte=today)
        )
        end_date_eq_due_gt_now = Q(
            due_date__gte=today,
            end_date__date=F("due_date"),
        )
        return not_completed & (
            no_recurrence
            | (has_recurrence & (end_date_lt_due_gt_now | end_date_eq_due_gt_now))
        )

    @classmethod
    def status_in_progress_query(cls):
        """Query for filtering in progress checklist"""
        return Q(completed=False, started_on__isnull=False)

    @classmethod
    def status_past_due_query(cls):
        """Query for filtering past due checklist"""
        return ~Q(run=None) & Q(
            completed=False,
            due_date__isnull=False,
            due_date__lt=timezone.now(),
        )

    @classmethod
    def status_up_coming_query(cls):
        """Query for filtering upcoming checklist"""
        return ~Q(run=None) & Q(
            completed=False,
            end_date__isnull=False,
            end_date__date__gt=timezone.now().date(),
            due_date__gt=F("end_date"),
        )

    @classmethod
    def status_completed_query(cls):
        """Query for filtering completed checklist"""
        return Q(completed=True)

    def filter_by_statuses(self, statuses, user=None):
        """Filter checklist by a list of statuses

        Args:
            statuses: str. List of statuses
            user: User. A user instance, or None.
        """
        queryset = self
        if user is not None:
            queryset = queryset.filter(assignee=user)
        query = Q()
        for status in statuses:
            get_query_method_name = f"status_{status.lower()}_query"
            get_query_method = getattr(self, get_query_method_name, None)
            assert (
                get_query_method is not None
            ), f"{get_query_method_name} needs to be existing and implemented"
            query |= get_query_method()
        return queryset.filter(query).distinct()

    def filter_by_assignees(self, usernames: list[str]):
        """
        Filter checklist by assignees usernames

        Args:
            usernames: list. Usernames list
        """
        return self.filter(
            Q(run=None, assignee__username__in=usernames)
            | Q(run__checklists__assignee__username__in=usernames)
        )

    def all_display_for_user(self, user: User):
        """
        All checklists that are used for display for a given user

        Args:
            user: str. User's username
        """
        team_ids = Team.objects.filter(members=user).distinct().values("id")
        user_checklist_query = Q(assignee=user) & (
            Q(run=None) | (~Q(run=None) & Q(run__team_id__in=team_ids))
        )
        other_run_checklist_query = (
            ~Q(run=None)
            & ~Q(run__checklists__assignee=user)
            & Q(run__team_id__in=team_ids)
        )
        query = user_checklist_query | other_run_checklist_query
        return self.filter(query)


class ChecklistManager(models.Manager):
    """Checklist manager"""

    def sort_by_run_info(self):
        """Sort by run-relevant information."""
        return self.order_by("due_date", "created_on")

    def get_queryset(self) -> models.QuerySet:
        """Override to change default queryset for checklist"""
        return ChecklistQuerySet(self.model, using=self._db)
