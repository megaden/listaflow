"""
Factories for Workflow module tests.
"""
from django.utils import timezone

from django_celery_beat.models import MINUTES, IntervalSchedule
from factory import Faker, LazyFunction, SelfAttribute, SubFactory, post_generation
from factory.django import DjangoModelFactory

from profiles.tests.factories import UserFactory
from workflow.models import (
    Checklist,
    ChecklistDefinition,
    ChecklistTask,
    ChecklistTaskDefinition,
    Email,
    Recurrence,
    Run,
)


class ChecklistDefinitionFactory(DjangoModelFactory):
    """
    ChecklistDefinition factory.
    """

    class Meta:
        model = ChecklistDefinition

    name = Faker("name")
    body = Faker("paragraph")
    author = SubFactory(UserFactory)


class ChecklistTaskDefinitionFactory(DjangoModelFactory):
    """
    ChecklistTaskDefinition factory.
    """

    class Meta:
        model = ChecklistTaskDefinition

    checklist_definition = SubFactory(ChecklistDefinitionFactory)
    label = Faker("name")
    body = Faker("paragraph")
    required = Faker("pybool")

    @post_generation
    def tags(self, create, extracted, **kwargs):
        """
        Create and assign tags upon instance creation.
        """
        if not create:
            return

        if extracted:
            # pylint: disable=no-member
            self.tags.add(*extracted)


class ChecklistFactory(DjangoModelFactory):
    """
    Checklist Factory.
    """

    class Meta:
        model = Checklist

    definition = SubFactory(ChecklistDefinitionFactory)
    name = Faker("name")
    assignee = SubFactory(UserFactory)


class ChecklistTaskFactory(DjangoModelFactory):
    """
    Task factory.
    """

    class Meta:
        model = ChecklistTask

    definition = SubFactory(ChecklistTaskDefinitionFactory)
    checklist = SubFactory(
        ChecklistFactory, definition=SelfAttribute("..definition.checklist_definition")
    )


class RunFactory(DjangoModelFactory):
    """
    Run factory.
    """

    class Meta:
        model = Run

    start_date = Faker("date")
    end_date = Faker("date")


class IntervalScheduleFactory(DjangoModelFactory):
    """
    IntervalSchedule factory.
    """

    class Meta:
        model = IntervalSchedule

    every = 1
    period = MINUTES


class RecurrenceFactory(DjangoModelFactory):
    """
    Recurrence factory.
    """

    class Meta:
        model = Recurrence

    start_date = LazyFunction(timezone.now)
    interval_schedule = SubFactory(IntervalScheduleFactory)


class EmailFactory(DjangoModelFactory):
    """
    email templates factory
    """

    class Meta:
        model = Email

    subject = Faker("name")
    text_body = Faker("paragraph")
    html_body = Faker("paragraph")
