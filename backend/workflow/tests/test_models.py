"""
Tests for Workflow models.
"""
from datetime import timedelta
from unittest.mock import patch

from django.core.exceptions import ValidationError
from django.utils import timezone

import pytest
from django_celery_beat.models import HOURS, PeriodicTask

from backend.workflow.tests.utils import create_team_and_checklist_definition
from lib.models import Tag
from lib.tests.factories import TagFactory
from profiles.tests.factories import UserFactory
from workflow.models import Checklist, ChecklistTask, Recurrence, Run
from workflow.tests.factories import (
    ChecklistDefinitionFactory,
    ChecklistFactory,
    ChecklistTaskDefinitionFactory,
    ChecklistTaskFactory,
    IntervalScheduleFactory,
    RecurrenceFactory,
)

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("celery_worker", "patch_flush_many_cache"),
]


def test_partial_checklist_completion():
    """
    If we didn't complete all tasks, the complete
    function should not update completed flag
    """
    checklist = ChecklistFactory()
    tasks = [ChecklistTaskFactory(checklist=checklist) for _ in range(3)]
    definition = ChecklistTaskDefinitionFactory(required=True)
    tasks.append(ChecklistTaskFactory(checklist=checklist, definition=definition))
    checklist.refresh_from_db()
    assert checklist.completed is False
    assert checklist.completed_on is None
    tasks[0].completed = True
    tasks[0].save()
    status, _ = checklist.complete()
    assert status is False
    checklist.refresh_from_db()
    assert checklist.completed is False


def test_full_checklist_completion():
    """
    If all tasks are completed, the complete function should update completed flag.
    """
    checklist = ChecklistFactory()
    tasks = [ChecklistTaskFactory(checklist=checklist) for _ in range(3)]
    for task in tasks:
        task.completed = True
        task.save()
    status, _ = checklist.complete()
    assert status is True
    checklist.refresh_from_db()
    assert checklist.completed is True
    assert checklist.completed_on is not None


def test_required_checklist_completion():
    """
    If all required tasks are completed, complete function should update completed flag.
    """
    checklist = ChecklistFactory()
    tasks = [ChecklistTaskFactory(checklist=checklist) for _ in range(3)]
    # atleast one required task
    definition = ChecklistTaskDefinitionFactory(required=True)
    tasks.append(ChecklistTaskFactory(checklist=checklist, definition=definition))
    for task in tasks:
        task.definition.active = True
        if task.required:
            task.completed = True
            task.save()
    status, _ = checklist.complete()
    assert status is True
    checklist.refresh_from_db()
    assert checklist.completed is True
    assert checklist.completed_on is not None


def test_rollback_completion():
    """
    If a required task is unchecked, remove completion settings from checklist.
    """
    definition = ChecklistTaskDefinitionFactory(required=True)
    checklist = ChecklistFactory(completed=True, completed_on=timezone.now())
    checklist.refresh_from_db()
    assert checklist.completed is True
    assert checklist.completed_on is not None
    ChecklistTaskFactory(definition=definition, checklist=checklist)
    checklist.refresh_from_db()
    assert checklist.completed is False
    assert checklist.completed_on is None


def test_task_completed_on():
    """
    Test setting the completed attribute on a task also marks the completed_on
    attribute.
    """
    task = ChecklistTaskFactory()
    assert task.completed_on is None
    task.completed = True
    task.save()
    task.refresh_from_db()
    assert task.completed_on is not None


def test_task_unchecked_removes_completed_on():
    """
    Test that unchecking a task also removes its completed_on stamp.
    """
    task = ChecklistTaskFactory(completed=True)
    task.refresh_from_db()
    assert task.completed_on is not None
    assert task.completed is True
    task.completed = False
    task.save()
    assert task.completed_on is None
    assert task.completed is False


def check_empty():
    """
    Assert that the database doesn't have any checklists or checklist tasks.
    """
    checklist_models = Checklist.objects.all()
    checklist_task_models = ChecklistTask.objects.all()
    assert len(checklist_models) == 0
    assert len(checklist_task_models) == 0


def test_create_checklist_and_corresponding_tasks_creates_checklist_and_tasks():
    """Test calling Checklist.create_checklist_and_corresponding_tasks creates
    checklist and task models.
    """
    # Baseline verification.
    checklist_models = Checklist.objects.all()
    checklist_task_models = ChecklistTask.objects.all()
    assert len(checklist_models) == 0
    assert len(checklist_task_models) == 0

    # Setup.
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)

    # Action.
    checklist, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee, checklist_definition
    )

    # End verification.
    checklist_models = Checklist.objects.all()
    checklist_task_models = list(ChecklistTask.objects.all())
    assert checklist_models.get() == checklist
    assert sorted(checklist_task_models) == sorted(tasks)


def test_create_checklist_and_corresponding_skips_nonmatching_tasks():
    """Test calling Checklist.create_checklist_and_corresponding_tasks skips tasks that
    don't match tag specifications.
    """
    # Baseline verification.
    check_empty()

    # Setup.
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        tags=["test"],
    )

    # Action.
    checklist, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
        user_tags=[TagFactory()],
    )

    # End verification.
    check_empty()
    assert checklist is None
    assert not tasks


def test_create_checklist_creates_tasks_for_match():
    """Test calling Checklist.create_checklist_and_corresponding_tasks creates
    checklists when the tags match
    """
    # Baseline verification.
    check_empty()

    # Setup.
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        tags=["test"],
    )

    # Action.
    checklist, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
        user_tags=Tag.objects.all(),
    )

    # End verification.
    checklist_models = Checklist.objects.all()
    checklist_task_models = list(ChecklistTask.objects.all())
    assert checklist_models.get() == checklist
    assert sorted(checklist_task_models) == sorted(tasks)
    assert tasks[0].tags.all()[0] == Tag.objects.get()


@pytest.mark.parametrize("due_days_no", [1, 2, 3])
def test_create_run_for_team_creates_run_model(due_days_no):
    """
    Test calling Run.create_run_for_team method creates Run model with correct
    values
    """
    # Baseline verification.
    models = Run.objects.all()
    assert len(models) == 0

    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(
        team=team,
        checklist_definition=checklist_definition,
        due_days_no=due_days_no,
    )

    # Action.
    Run.create_run_for_team(team, checklist_definition, recurrence)

    # End verification.
    models = Run.objects.all()
    assert len(models) == 1
    due_date = models[0].due_date
    expected_due_date = timezone.now().date() + timezone.timedelta(due_days_no)
    assert due_date == expected_due_date


@patch("workflow.models.Checklist.create_checklist_and_corresponding_tasks")
def test_create_run_for_team_calls_create_checklist_and_corresponding_tasks(
    mock_verify,
):
    """Test calling Run.create_run_for_team method calls
    Checklist.create_checklist_and_corresponding_tasks.
    """
    # Baseline verification.
    assert not mock_verify.called

    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    mock_verify.return_value = ChecklistFactory(), [ChecklistTaskFactory()]

    # Action.
    Run.create_run_for_team(team, checklist_definition)

    # End verification.
    assert mock_verify.called


def test_creating_recurrence_model_creates_periodic_task_model():
    """Test to ensure creating a recurrence model creates a PeriodicTask model."""
    # Baseline verification.
    models = PeriodicTask.objects.all()
    assert len(models) == 0

    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()

    # Action.
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )

    # End verification.
    periodic_task_models = PeriodicTask.objects.all()
    assert len(periodic_task_models) == 1
    assert periodic_task_models[0].interval.id == recurrence_model.interval_schedule.id


def test_creating_recurrence_interval_schedule_model_sets_last_run_at():
    """Test to ensure creating a recurrence model with interval sets last_run_at"""
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    interval = IntervalScheduleFactory(period=HOURS)
    start_date = timezone.now() + timezone.timedelta(hours=1)

    # Action.
    Recurrence.objects.create(
        start_date=start_date,
        interval_schedule=interval,
        team=team,
        checklist_definition=checklist_definition,
    )

    # verification.
    periodic_task_model = PeriodicTask.objects.first()
    actual_start_time = timezone.now() + timezone.timedelta(
        seconds=periodic_task_model.schedule.is_due(timezone.now()).next
    )
    allowed_delta = timezone.timedelta(seconds=1)
    assert abs(actual_start_time - start_date) < allowed_delta


def test_creating_recurrence_interval_schedule_model_with_past_start_date():
    """Test to ensure creating a recurrence model with past start date raises"""
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    interval = IntervalScheduleFactory(period=HOURS)
    start_date = timezone.now() - timezone.timedelta(hours=1)

    # Action.
    with pytest.raises(ValidationError, match="Start date cannot be set in past"):
        Recurrence.objects.create(
            start_date=start_date,
            interval_schedule=interval,
            team=team,
            checklist_definition=checklist_definition,
        )


def test_updating_recurrence_interval_schedule_does_change_last_run_at():
    """Test to ensure updating an interval_schedule does change last_run_at"""
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    interval = IntervalScheduleFactory(period=HOURS)
    start_date = timezone.now()

    recurrence = Recurrence.objects.create(
        start_date=start_date,
        interval_schedule=interval,
        team=team,
        checklist_definition=checklist_definition,
    )
    last_run_at = recurrence.periodic_task.last_run_at
    interval_new = IntervalScheduleFactory(every=30)
    recurrence.interval_schedule = interval_new
    recurrence.save()

    # verify
    assert recurrence.periodic_task.last_run_at != last_run_at


def test_updating_recurrence_team__model_does_not_change_last_run_at():
    """Test to ensure updating a recurrence model team does not change last_run_at"""
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    interval = IntervalScheduleFactory(period=HOURS)
    start_date = timezone.now()

    recurrence = Recurrence.objects.create(
        start_date=start_date,
        interval_schedule=interval,
        team=team,
        checklist_definition=checklist_definition,
    )
    last_run_at = recurrence.periodic_task.last_run_at
    new_team, _, _ = create_team_and_checklist_definition(username_prefix="new")
    recurrence.team = new_team
    recurrence.save()

    # verify
    assert recurrence.periodic_task.last_run_at == last_run_at


def test_creating_recurrence_model_adds_correct_task_in_creates_periodic_task_model():
    """Test to ensure creating a recurrence model adds correct task in the newly created
    PeriodicTask model.
    """
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()

    # Action.
    RecurrenceFactory.create(team=team, checklist_definition=checklist_definition)

    # End verification.
    periodic_task_models = PeriodicTask.objects.all()
    assert len(periodic_task_models) == 1
    assert periodic_task_models[0].task == "workflow.tasks.create_checklist_for_users"


def test_updating_a_recurrence_model_does_not_creates_new_periodic_task_model():
    """Test to ensure updating a recurrence model does not creates new
    PeriodicTask model.
    """
    # Baseline setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )

    # Baseline verification.
    models = PeriodicTask.objects.all()
    assert len(models) == 1

    # Action.
    recurrence_model.checklist_definition = ChecklistDefinitionFactory()
    recurrence_model.save()

    # End verification.
    periodic_task_models = PeriodicTask.objects.all()
    assert len(periodic_task_models) == 1


def test_deactivating_recurrence_model_disables_periodic_task_model():
    """Test to ensure deactivating a recurrence model disables PeriodicTask
    model.
    """
    # Baseline setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )

    # Baseline verification.
    assert recurrence_model.active
    assert recurrence_model.periodic_task.enabled

    # Action.
    recurrence_model.active = False
    recurrence_model.save()

    # End verification.
    recurrence_model.refresh_from_db()
    assert not recurrence_model.active
    assert not recurrence_model.periodic_task.enabled


def test_checklist_get_all_display_for_user():
    """Test get all checklist for displaying purpose of a given user"""
    # Setup
    team1, definition1, _task_definitions = create_team_and_checklist_definition(
        username_prefix="team1",
        team_member_count=3,
    )
    team2, definition2, _task_definitions = create_team_and_checklist_definition(
        username_prefix="team2",
        team_member_count=4,
    )

    # Add team2's member to team1
    user_team1 = team1.members.first()
    user_team2 = team2.members.first()
    team1.members.add(user_team2)
    team1.save()

    # One off
    ChecklistFactory.create(assignee=user_team1)
    ChecklistFactory.create(assignee=user_team2)

    # Run checklists
    Run.create_run_for_team(team1, definition1)
    Run.create_run_for_team(team2, definition2)

    queryset = Checklist.objects.get_queryset()

    # Action
    # Checklists of other team not included
    # user in 1 team, 1 one-off + 1 recurring
    assert queryset.all_display_for_user(user_team1).count() == 2
    # user in 2 teams, 1 one-off + 2 recurring
    assert queryset.all_display_for_user(user_team2).count() == 3


def test_sort_by_run_due_date_for_checklist():
    """Test sort by run_due_date for checklist"""
    team, definition, _task_definitions = create_team_and_checklist_definition()
    oldest_run, middle_run, latest_run = tuple(
        Run.create_run_for_team(team, definition) for _ in range(3)
    )
    now_dt = timezone.now().date()

    oldest_run.due_date = now_dt - timedelta(days=1)
    oldest_run.save()

    middle_run.due_date = now_dt
    middle_run.save()

    latest_run.due_date = now_dt + timedelta(days=1)
    latest_run.save()

    queryset = Checklist.objects.sort_by_run_info()
    first_cl = queryset.first()
    last_cl = queryset.last()
    assert first_cl.due_date == oldest_run.due_date
    assert last_cl.due_date == latest_run.due_date


def test_run_stats():
    """
    Test get stats of Run
    """
    (
        team,
        checklist_definition,
        _test_definitions,
    ) = create_team_and_checklist_definition()
    ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)
    run = Run.create_run_for_team(team, checklist_definition)

    checklists = run.checklists.all()
    assert len(checklists) == 2


def test_run_label():
    """
    Test run instance __str__ representation
    """
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)

    # test with no recurrence instance, start and end date should be now
    run_instance = Run.create_run_for_team(team, checklist_definition)
    expected_label = "Run, <checklist definition:" f" {checklist_definition}>"
    assert str(run_instance) == expected_label

    # test with recurrence instance, end date should be start of next run
    interval = IntervalScheduleFactory(period=HOURS)
    recurrence = Recurrence.objects.create(
        interval_schedule=interval,
        team=team,
        checklist_definition=checklist_definition,
    )

    run_instance = Run.create_run_for_team(team, checklist_definition, recurrence)
    expected_label = (
        f"Run for {recurrence.id}, <checklist definition:" f" {checklist_definition}>"
    )
    assert str(run_instance) == expected_label


def test_checklist_task_trends():
    """
    Test checklist task trend
    """
    # Baseline setup.
    (
        team,
        checklist_definition,
        task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    runs = [Run.create_run_for_team(team, checklist_definition, recurrence_model)]

    # Action
    assert len(checklist_definition.all_tasks_trends(runs, team.members.all())) == 1
    assert len(checklist_definition.related_runs()) == 1

    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition, active=True
    )
    runs.append(Run.create_run_for_team(team, checklist_definition, recurrence_model))
    assert len(checklist_definition.all_tasks_trends(runs, team.members.all())) == 2
    assert len(checklist_definition.related_runs()) == 2

    task_definitions[0].active = False
    task_definitions[0].save()
    assert len(checklist_definition.all_tasks_trends(runs, team.members.all())) == 1

    checklist_definition.refresh_from_db()
    assert len(checklist_definition.all_tasks_trends(runs, team.members.all())) == 1
