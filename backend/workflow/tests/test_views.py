"""
Tests for workflow module.
"""
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse

import pytest
from faker import Faker
from rest_framework import status
from rest_framework.exceptions import ValidationError

from profiles.tests.factories import TeamFactory, UserFactory
from workflow.enums import ChecklistListName
from workflow.interaction import (
    LINEAR_SCALE_RATING_INTERFACE,
    NUMERIC_INTERFACE,
    get_interfaces_customization_arg_default_value,
)
from workflow.models import Checklist, Run, ChecklistTask
from workflow.report import (
    REPORT_FORMAT_CSV,
    REPORT_FORMAT_PDF,
    REPORT_FORMAT_QUERY_KEY,
)
from workflow.report.csv import TemplateRunsCsvReport
from workflow.report.pdf import TemplateRunsPdfReport
from workflow.tests.data import INTERFACE_SCENARIOS
from workflow.tests.factories import (
    ChecklistDefinitionFactory,
    ChecklistFactory,
    ChecklistTaskDefinitionFactory,
    ChecklistTaskFactory,
    RecurrenceFactory,
)

from workflow.tests.utils import create_team_and_checklist_definition
from workflow.views import ChecklistDefinitionViewSet, ReportViewMixin

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("api_client", "celery_worker", "patch_flush_many_cache"),
]

fake = Faker()


def test_report_view_mixin():
    """
    Test ReportViewMixin class
    """
    # Setup
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(tasks_kwargs=[{}, {}])
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    for _ in range(2):
        ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)
    run = Run.create_run_for_team(team, checklist_definition, recurrence)
    tasks = ChecklistDefinitionViewSet.related_checklist_tasks(
        checklist_definition, [run], team.members.all()
    )
    run_csv_report = TemplateRunsCsvReport(instance=tasks, is_many=True)
    run_csv_filename = "run_csv_report.csv"

    tasks = ChecklistDefinitionViewSet.related_checklist_tasks(
        checklist_definition, [run], team.members.all()
    )
    run_pdf_report = TemplateRunsPdfReport(instance=tasks, is_many=True)
    run_pdf_filename = "run_csv_report.pdf"

    # Action
    response = ReportViewMixin.get_csv_report_repsonse(run_csv_filename, run_csv_report)
    headers = response.headers
    assert headers["Content-Type"] == "text/csv"

    response = ReportViewMixin.get_pdf_report_repsonse(run_pdf_filename, run_pdf_report)
    headers = response.headers
    assert headers["Content-Type"] == "application/pdf"


def test_team_filter_no_team_ids():
    """
    test team filter when no team_id is passed
    """
    user = UserFactory()
    team = TeamFactory()
    with pytest.raises(ValidationError):
        ChecklistDefinitionViewSet.get_allowed_team_ids(user, [])

    team.members.add(user)
    team_ids = ChecklistDefinitionViewSet.get_allowed_team_ids(user, [])
    assert team_ids == [team.id]


def test_team_filter_unauthorized_team_access():
    """
    test team filter when user is not part of team
    """
    user = UserFactory()
    team = TeamFactory()
    team2 = TeamFactory()
    team.members.add(user)
    with pytest.raises(ValidationError):
        ChecklistDefinitionViewSet.get_allowed_team_ids(user, [team2.id])


def test_team_filter_superuser_team_access():
    """
    test team filter superuser tries to access all teams
    """
    user = UserFactory()
    user.is_superuser = True
    user.save()
    team = TeamFactory()
    team2 = TeamFactory()
    team.members.add(user)
    team_ids = ChecklistDefinitionViewSet.get_allowed_team_ids(
        user, [team.id, team2.id]
    )
    assert team_ids == [team.id, team2.id]


def test_fetch_checklist_short_definitions(*, api_client):
    """
    Test listing of checklist definitions in short.
    """
    client = api_client()
    user = UserFactory()
    definitions = [ChecklistDefinitionFactory() for _ in range(3)]
    for _ in range(3):
        # These should not appear in the listing.
        ChecklistDefinitionFactory(active=False)
    client.force_login(user)
    response = client.get(reverse("workflow:short-definition-list"))
    assert response.status_code == status.HTTP_200_OK
    assert sorted([item["id"] for item in response.data]) == sorted(
        [definition.id for definition in definitions]
    )


def test_fetch_checklist_definitions(*, api_client):
    """
    Test listing of checklist definitions.
    """
    client = api_client()
    user = UserFactory()
    definitions = [ChecklistDefinitionFactory() for _ in range(3)]
    for _ in range(3):
        # These should not appear in the listing.
        ChecklistDefinitionFactory(active=False)
    client.force_login(user)
    response = client.get(reverse("workflow:checklistdefinition-list"))
    assert response.status_code == status.HTTP_200_OK
    assert sorted([item["id"] for item in response.data["results"]]) == sorted(
        [definition.id for definition in definitions]
    )


def test_create_checklist_definition(*, api_client):
    """
    Test creation of a checklist definition.
    """
    client = api_client()
    user = UserFactory()
    client.force_login(user)
    name = fake.name()
    body = fake.paragraph()
    response = client.post(
        reverse("workflow:checklistdefinition-list"), {"name": name, "body": body}
    )
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data["name"] == name
    assert response.data["body"] == body


def test_create_checklist_definition_fails_not_logged_in(*, api_client):
    """
    Test that a login is required for creating a checklist.
    """
    client = api_client()
    response = client.post(
        reverse("workflow:checklistdefinition-list"), {"name": "test", "body": "test"}
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_checklist_definition_update(*, api_client):
    """
    Test updating a checklist definition.
    """
    client = api_client()
    user = UserFactory()
    checklist_definition = ChecklistDefinitionFactory(author=user)
    client.force_login(user)
    response = client.patch(
        reverse(
            "workflow:checklistdefinition-detail",
            kwargs={"id": checklist_definition.id},
        ),
        {"name": "New Name"},
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.data["name"] == "New Name"


def test_checklist_definition_update_requires_right_user(*, api_client):
    """
    Test that the right user is required for updating a checklist.
    """
    client = api_client()
    user = UserFactory()
    checklist_definition = ChecklistDefinitionFactory()
    client.force_login(user)
    response = client.patch(
        reverse(
            "workflow:checklistdefinition-detail",
            kwargs={"id": checklist_definition.id},
        ),
        {"name": "New Name"},
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_update_checklist_requires_authentication(*, api_client):
    """
    Test that an unlogged in user can't modify a checklist.
    """
    client = api_client()
    checklist_definition = ChecklistDefinitionFactory()
    response = client.patch(
        reverse(
            "workflow:checklistdefinition-detail",
            kwargs={"id": checklist_definition.id},
        ),
        {"name": "New Name"},
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_list_checklist_definition_tasks_on_checklist_definition(*, api_client):
    """
    Verify listing of task definitions on a checklist definition.
    """
    client = api_client()
    checklist_definition = ChecklistDefinitionFactory()
    client.force_login(checklist_definition.author)
    task_definitions = [
        ChecklistTaskDefinitionFactory(checklist_definition=checklist_definition)
        for _ in range(3)
    ]
    for _ in range(3):
        # These should not appear in the listing.
        ChecklistTaskDefinitionFactory(
            checklist_definition=checklist_definition, active=False
        )
    response = client.get(
        reverse(
            "workflow:checklistdefinition-detail",
            kwargs={"id": checklist_definition.id},
        )
    )
    assert response.status_code == status.HTTP_200_OK
    assert sorted([task["id"] for task in response.data["task_definitions"]]) == sorted(
        [task.id for task in task_definitions]
    )


def test_list_checklist_definition_tasks(*, api_client):
    """
    Verify listing of task definitions on the listing endpoint for a checklist
    definition.
    """
    client = api_client()
    checklist_definition = ChecklistDefinitionFactory()
    client.force_login(checklist_definition.author)
    task_definitions = [
        ChecklistTaskDefinitionFactory(checklist_definition=checklist_definition)
        for _ in range(3)
    ]
    for _ in range(3):
        # These should not appear in the listing.
        ChecklistTaskDefinitionFactory(
            checklist_definition=checklist_definition, active=False
        )
    response = client.get(
        reverse(
            "workflow:checklist-definition-task-definitions-list",
            kwargs={"checklist_definition_id": checklist_definition.id},
        )
    )
    assert response.status_code == status.HTTP_200_OK
    assert sorted([task["id"] for task in response.data]) == sorted(
        [task.id for task in task_definitions]
    )


def test_create_checklist_task_definition(*, api_client):
    """
    Verify that a task can be defined.
    """
    client = api_client()
    checklist_definition = ChecklistDefinitionFactory()
    client.force_login(checklist_definition.author)
    label = fake.name()
    body = fake.paragraph()
    response = client.post(
        reverse(
            "workflow:checklist-definition-task-definitions-list",
            kwargs={"checklist_definition_id": checklist_definition.id},
        ),
        {"label": label, "body": body},
    )
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data["label"] == label
    assert response.data["body"] == body


def test_create_checklist_task_definition_requires_author(*, api_client):
    """
    Verify that a task can be defined only by the author.
    """
    client = api_client()
    checklist_definition = ChecklistDefinitionFactory()
    user = UserFactory.create()
    client.force_login(user)
    response = client.post(
        reverse(
            "workflow:checklist-definition-task-definitions-list",
            kwargs={"checklist_definition_id": checklist_definition.id},
        ),
        {"label": "test", "body": "test"},
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_update_checklist_task_definition(*, api_client):
    """
    Test patching a task definition.
    """
    client = api_client()
    task_definition = ChecklistTaskDefinitionFactory()
    client.force_login(task_definition.author)
    response = client.patch(
        reverse(
            "workflow:checklist-definition-task-definitions-detail",
            kwargs={
                "checklist_definition_id": task_definition.checklist_definition.id,
                "id": task_definition.id,
            },
        ),
        {"label": "test label", "body": "test body"},
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.data["label"] == "test label"
    assert response.data["body"] == "test body"


def test_create_checklist(*, api_client):
    """
    Test the creation of a checklist from a definition.
    """
    client = api_client()
    checklist_definition = ChecklistDefinitionFactory()
    task_definitions = [
        ChecklistTaskDefinitionFactory(checklist_definition=checklist_definition)
        for _ in range(3)
    ]
    task_definitions[0].tags.set(["Test", "Test2"])
    for _ in range(3):
        # These tasks should not be created.
        ChecklistTaskDefinitionFactory(
            checklist_definition=checklist_definition, active=False
        )
    user = UserFactory()
    client.force_login(user)
    response = client.post(
        reverse("workflow:user-checklists-list", kwargs={"username": user.username}),
        {"definition": checklist_definition.id},
    )
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data["name"] == checklist_definition.name
    assert response.data["id"] != checklist_definition.id
    assert sorted([task["label"] for task in response.data["tasks"]]) == sorted(
        [task.label for task in task_definitions]
    )
    task = ChecklistTask.objects.get(definition=task_definitions[0])
    assert list(task.tags.all().order_by("id")) == list(task.tags.all().order_by("id"))


def test_retrive_task(*, api_client):
    """
    Test retrieving a task directly.
    """
    client = api_client()
    task = ChecklistTaskFactory()
    client.force_login(task.checklist.assignee)
    response = client.get(
        reverse(
            "workflow:checklist-tasks-detail",
            kwargs={
                "username": task.checklist.assignee.username,
                "checklist_id": task.checklist.id,
                "id": task.id,
            },
        ),
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.data["completed"] is False


def test_retrive_task_assignee_only(*, api_client):
    """
    Test that non-assignees can't retrieve a task.
    """
    client = api_client()
    task = ChecklistTaskFactory()
    client.force_login(UserFactory())
    response = client.get(
        reverse(
            "workflow:checklist-tasks-detail",
            kwargs={
                "username": task.checklist.assignee.username,
                "checklist_id": task.checklist.id,
                "id": task.id,
            },
        ),
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_delete_checklist_definition(*, api_client):
    """
    Check deletion of a checklist definition.
    """
    client = api_client()
    checklist_definition = ChecklistDefinitionFactory()
    client.force_login(checklist_definition.author)
    response = client.delete(
        reverse(
            "workflow:checklistdefinition-detail",
            kwargs={"id": checklist_definition.id},
        )
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    with pytest.raises(ObjectDoesNotExist):
        checklist_definition.refresh_from_db()


def test_delete_checklist_definition_author_only(*, api_client):
    """
    Verify that only the author can perform deletion of a checklist definition.
    """
    client = api_client()
    checklist_definition = ChecklistDefinitionFactory()
    client.force_login(UserFactory())
    response = client.delete(
        reverse(
            "workflow:checklistdefinition-detail",
            kwargs={"id": checklist_definition.id},
        )
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_delete_checklist_definition_has_dependencies(*, api_client):
    """
    Make sure deleting a checklist that has dependencies does not remove the checklist
    from the database, but marks it inactive.
    """
    client = api_client()
    checklist_definition = ChecklistDefinitionFactory()
    checklist = ChecklistFactory(definition=checklist_definition)
    client.force_login(checklist_definition.author)
    response = client.delete(
        reverse(
            "workflow:checklistdefinition-detail",
            kwargs={"id": checklist_definition.id},
        )
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    response = client.get(
        reverse(
            "workflow:checklistdefinition-detail",
            kwargs={"id": checklist_definition.id},
        )
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND
    # Will throw if we cascaded.
    checklist.refresh_from_db()
    checklist_definition.refresh_from_db()
    assert checklist_definition.active is False


def test_delete_checklist_task_definition(*, api_client):
    """
    Verify task definitions can be deleted.
    """
    client = api_client()
    task_definition = ChecklistTaskDefinitionFactory()
    client.force_login(task_definition.author)
    response = client.delete(
        reverse(
            "workflow:checklist-definition-task-definitions-detail",
            kwargs={
                "id": task_definition.id,
                "checklist_definition_id": task_definition.checklist_definition.id,
            },
        )
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    with pytest.raises(ObjectDoesNotExist):
        task_definition.refresh_from_db()


def test_delete_checklist_task_definition_author_only(*, api_client):
    """
    Verify task definitions can be deleted.
    """
    client = api_client()
    task_definition = ChecklistTaskDefinitionFactory()
    client.force_login(UserFactory())
    response = client.delete(
        reverse(
            "workflow:checklist-definition-task-definitions-detail",
            kwargs={
                "id": task_definition.id,
                "checklist_definition_id": task_definition.checklist_definition.id,
            },
        )
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_delete_checklist_task_definition_has_dependencies(*, api_client):
    """
    Verify that deleting a task definition that has dependent tasks does not
    destroy those tasks and marks the definition as inactive.
    """
    client = api_client()
    task_definition = ChecklistTaskDefinitionFactory()
    task = ChecklistTaskFactory(definition=task_definition)
    client.force_login(task_definition.author)
    response = client.delete(
        reverse(
            "workflow:checklist-definition-task-definitions-detail",
            kwargs={
                "id": task_definition.id,
                "checklist_definition_id": task_definition.checklist_definition.id,
            },
        )
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    response = client.get(
        reverse(
            "workflow:checklist-definition-task-definitions-detail",
            kwargs={
                "id": task_definition.id,
                "checklist_definition_id": task_definition.checklist_definition.id,
            },
        )
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND
    # Will raise if the delete cascaded.
    task.refresh_from_db()
    task_definition.refresh_from_db()
    assert task_definition.active is False


@pytest.mark.parametrize(
    "interface_type,customization_args,user_response_data,completed",
    INTERFACE_SCENARIOS,
)
def test_interface_validation(
    *, api_client, interface_type, customization_args, user_response_data, completed
):
    """
    Verify that updates to the 'response' field are handled sanely.
    """
    client = api_client()
    task_definition = ChecklistTaskDefinitionFactory(
        interface_type=interface_type,
        customization_args=customization_args,
    )
    task = ChecklistTaskFactory(definition=task_definition)
    client.force_login(task.checklist.assignee)
    response = client.patch(
        reverse(
            "workflow:checklist-tasks-detail",
            kwargs={
                "username": task.checklist.assignee.username,
                "checklist_id": task.checklist.id,
                "id": task.id,
            },
        ),
        {"response": user_response_data},
        format="json",
    )
    if completed is None:
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        return
    assert response.data["completed"] == completed
    if completed:
        assert response.data["completed_on"]
    else:
        assert response.data["completed_on"] is None


# pylint: disable=too-many-locals
def test_fetch_checklists_list(*, api_client):
    """
    Test fetch list of checklists
    """
    # Setup
    client = api_client()
    (
        team_first,
        definition_first,
        _task_definitions_first,
    ) = create_team_and_checklist_definition(
        username_prefix="first", team_member_count=3
    )
    (
        team_second,
        definition_second,
        _task_definitions_second,
    ) = create_team_and_checklist_definition(
        username_prefix="second", team_member_count=4
    )

    recurrence_team_first = RecurrenceFactory.create(
        team=team_first, checklist_definition=definition_first
    )
    recurrence_team_second = RecurrenceFactory.create(
        team=team_second, checklist_definition=definition_second
    )

    for _ in range(2):
        Run.create_run_for_team(team_first, definition_first, recurrence_team_first)
        Run.create_run_for_team(team_second, definition_second, recurrence_team_second)

    Run.create_run_for_team(team_second, definition_second, recurrence_team_second)

    user_team_first = team_first.members.first()
    user_team_second = team_second.members.first()

    user_second_checklist = Checklist.objects.filter(assignee=user_team_second).first()
    user_second_checklist.completed = True
    user_second_checklist.save()

    url = reverse("workflow:checklists-list")
    # check only authenticated requests are allowed

    resp = client.get(url)
    assert resp.status_code == status.HTTP_401_UNAUTHORIZED

    # Action
    client.force_login(user_team_first)
    response = client.get(url)
    assert response.data["count"] == 2

    team_second.members.add(user_team_first)
    team_second.save()

    # Only checklists associated to the team are counted
    client.force_login(user_team_second)
    response = client.get(f"{url}?list_name={str(ChecklistListName.ALL)}")
    assert response.data["count"] == 3

    response = client.get(f"{url}?list_name={str(ChecklistListName.ASSIGNED_TO_ME)}")
    assert response.data["count"] == 3

    response = client.get(f"{url}?list_name={str(ChecklistListName.TO_DO)}")
    assert response.data["count"] == 2


def test_fetch_checklists_count(*, api_client):
    """Test fetch checklists count"""
    # Setup
    client = api_client()
    team, definition, _task_definitions = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(team=team, checklist_definition=definition)
    active_runs = [
        Run.create_run_for_team(team, definition, recurrence) for _ in range(3)
    ]
    archived_runs = [
        Run.create_run_for_team(team, definition, recurrence) for _ in range(3)
    ]

    for run in archived_runs:
        checklists = run.checklists.all()
        for checklist in checklists:
            checklist.is_archived = True
            checklist.save()
    url = reverse("workflow:checklists-count")

    # Action
    user = team.members.first()
    client.force_login(user)
    response = client.get(url)
    assert response.data["archived"][str(ChecklistListName.TO_DO)] == 3
    assert response.data["archived"][str(ChecklistListName.ASSIGNED_TO_ME)] == 3
    assert response.data["archived"][str(ChecklistListName.ALL)] == 3

    assert response.data["active"][str(ChecklistListName.TO_DO)] == 3
    assert response.data["active"][str(ChecklistListName.ASSIGNED_TO_ME)] == 3
    assert response.data["active"][str(ChecklistListName.ALL)] == 3

    active_run = active_runs[0]
    for checklist in active_run.checklists.all():
        checklist.completed = True
        checklist.save()

    for run in archived_runs:
        checklists = run.checklists.all()
        for checklist in checklists:
            checklist.completed = True
            checklist.save()

    response = client.get(url)
    assert response.data["archived"][str(ChecklistListName.TO_DO)] == 0
    assert response.data["archived"][str(ChecklistListName.ASSIGNED_TO_ME)] == 3
    assert response.data["archived"][str(ChecklistListName.ALL)] == 3

    assert response.data["active"][str(ChecklistListName.TO_DO)] == 2
    assert response.data["active"][str(ChecklistListName.ASSIGNED_TO_ME)] == 3
    assert response.data["active"][str(ChecklistListName.ALL)] == 3


def test_fetch_recurrence_list(*, api_client):
    """
    Test fetch active recurrence list
    """
    client = api_client()
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    url = reverse("workflow:checklist-recurrence-list")

    response = client.get(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED

    user = UserFactory()
    client.force_login(user)

    response = client.get(url)
    assert response.data["count"] == 1

    recurrence.active = False
    recurrence.save()

    response = client.get(url)
    assert response.data["count"] == 1


def _setup_team_run_with_tags(checklist_definition, team):
    """
    Helper function to setup run and team with 2 tags.
    """
    membership = team.teammembership_set.all()
    membership[0].tags.set(["1"])
    membership[1].tags.set(["2"])
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    run = Run.create_run_for_team(team, checklist_definition, recurrence)
    return run


def test_fetch_trends_report(*, api_client):
    """
    Test fetch trends report for Recurrence
    """
    client = api_client()
    (
        team,
        checklist_definition,
        task_definitions,
    ) = create_team_and_checklist_definition(
        team_member_count=2, tasks_kwargs=[{"tags": ["1"]}, {"tags": ["2"]}]
    )
    _setup_team_run_with_tags(checklist_definition, team)
    url = reverse(
        "workflow:checklistdefinition-trends",
        kwargs={
            "id": checklist_definition.id,
        },
    )
    response = client.get(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED

    user = UserFactory()
    client.force_login(user)
    team.members.add(user)

    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data["trends"]) == len(task_definitions)

    # Filter by tags
    response = client.get(url, {"tags": 1})
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data["trends"]) == 1

    invalid_report_format_url = f"{url}?{REPORT_FORMAT_QUERY_KEY}=dummy_format"
    response = client.get(invalid_report_format_url)
    assert response.status_code == status.HTTP_400_BAD_REQUEST

    csv_report_format_url = f"{url}?{REPORT_FORMAT_QUERY_KEY}={REPORT_FORMAT_CSV}"
    response = client.get(csv_report_format_url)
    assert response.headers["Content-Type"] == "text/csv"

    pdf_report_format_url = f"{url}?{REPORT_FORMAT_QUERY_KEY}={REPORT_FORMAT_PDF}"
    response = client.get(pdf_report_format_url)
    assert response.headers["Content-Type"] == "application/pdf"

    checklist_definition.active = False
    checklist_definition.save()
    response = client.get(url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_fetch_trends_report_for_empty_recurrence_has_numeric_task_definition(
    *, api_client
):
    """
    Make sure aggregation of numeric interfaces work for empty recurrence
    """
    # Setup
    client = api_client()
    default_customization_args = get_interfaces_customization_arg_default_value()
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(
        tasks_kwargs=[
            {
                "interface_type": LINEAR_SCALE_RATING_INTERFACE,
                "customization_args": default_customization_args[
                    LINEAR_SCALE_RATING_INTERFACE
                ],
            },
            {
                "interface_type": NUMERIC_INTERFACE,
                "customization_args": default_customization_args[NUMERIC_INTERFACE],
            },
        ]
    )
    RecurrenceFactory.create(team=team, checklist_definition=checklist_definition)
    url = reverse(
        "workflow:checklistdefinition-trends",
        kwargs={
            "id": checklist_definition.id,
        },
    )
    user = UserFactory()
    client.force_login(user)
    team.members.add(user)

    # Action
    csv_report_format_url = f"{url}?{REPORT_FORMAT_QUERY_KEY}={REPORT_FORMAT_CSV}"
    response = client.get(csv_report_format_url)
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["Content-Type"] == "text/csv"

    pdf_report_format_url = f"{url}?{REPORT_FORMAT_QUERY_KEY}={REPORT_FORMAT_PDF}"
    response = client.get(pdf_report_format_url)
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["Content-Type"] == "application/pdf"


def test_fetch_run_list(*, api_client):
    """
    Test fetch run list of a recurrence
    """
    client = api_client()
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )

    Run.create_run_for_team(team, checklist_definition, recurrence)
    url = reverse(
        "workflow:checklist-recurrence-run-list",
        kwargs={"recurrence_id": recurrence.id},
    )
    response = client.get(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED

    user = UserFactory()
    client.force_login(user)

    response = client.get(url)
    assert response.data["count"] == 1

    recurrence.active = False
    recurrence.save()
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK


def test_compare_run(*, api_client):
    """
    Test compare Recurrence Runs
    """
    client = api_client()
    (team, checklist_definition, _,) = create_team_and_checklist_definition(
        team_member_count=2, tasks_kwargs=[{"tags": ["1"]}, {"tags": ["2"]}]
    )
    run = _setup_team_run_with_tags(checklist_definition, team)
    url = reverse(
        "workflow:checklistdefinition-compare",
        kwargs={"id": checklist_definition.id},
    )
    response = client.get(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED

    user = UserFactory()
    team.members.add(user)
    client.force_login(user)
    query_params = {
        "start_date": run.start_date.date(),
        "end_date": run.start_date.date(),
    }

    response = client.get(url, query_params)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 2

    # Filter by tags
    query_params["tags"] = "1"
    response = client.get(url, query_params)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 1

    query_params[REPORT_FORMAT_QUERY_KEY] = REPORT_FORMAT_CSV
    response = client.get(url, query_params)
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["Content-Type"] == "text/csv"


def test_checklist_definition_related_tags_view(*, api_client):
    """
    Test checklist definition related_tags view.
    """
    client = api_client()
    (team, checklist_definition, _,) = create_team_and_checklist_definition(
        team_member_count=2, tasks_kwargs=[{"tags": ["1"]}, {"tags": ["2"]}]
    )
    _setup_team_run_with_tags(checklist_definition, team)
    url = reverse(
        "workflow:checklistdefinition-related-tags",
        kwargs={"id": checklist_definition.id},
    )
    user = UserFactory()
    team.members.add(user)
    client.force_login(user)
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 2
