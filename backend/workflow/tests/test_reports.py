"""
Test for workflow report
"""
from datetime import datetime
from io import StringIO

import pytest

from workflow.interaction import (
    CHECKBOX_INTERFACE,
    LINEAR_SCALE_RATING_INTERFACE,
    MARKDOWN_TEXTAREA_INTERFACE,
    NUMERIC_INTERFACE,
    get_interfaces_customization_arg_default_value,
)
from workflow.models import Checklist, ChecklistTask, Run, RunTrendTaskReport
from workflow.report.csv import (
    ChecklistDefinitionTrendsCsvReport,
    TemplateRunsCsvReport,
)
from workflow.report.json import ChecklistDefinitionTrendsJsonReport
from workflow.report.pdf import (
    ChecklistDefinitionTrendsPdfReport,
    TemplateRunsPdfReport,
)
from workflow.serializers import ChecklistDefinitionTrendsSerializer
from workflow.tests.factories import RecurrenceFactory
from workflow.tests.utils import (
    create_checklist_task_definition,
    create_team_and_checklist_definition,
)
from workflow.views import ChecklistDefinitionViewSet

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("celery_worker", "patch_flush_many_cache"),
]


def test_recurrence_trends_json_report():
    """
    Test generate PDF report of Recurrence Task Trends
    """
    # Setup
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(tasks_kwargs=[{}, {}])

    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    recurrence.save()
    for _ in range(2):
        Run.create_run_for_team(
            recurrence.team, recurrence.checklist_definition, recurrence
        )
    runs = Run.objects.all()
    context = {"runs": runs, "users": team.members.values_list("id", flat=True)}

    # Action
    trends_report = ChecklistDefinitionTrendsJsonReport(
        instance=checklist_definition, context=context
    )
    serialized_data = ChecklistDefinitionTrendsSerializer(
        instance=checklist_definition,
        context=context,
    ).data
    assert trends_report.get_json_data() == serialized_data


def test_recurrence_run_report_csv():
    """
    Test generating CSV format of recurrence run report
    """
    team, checklist_definition, task_definitions = create_team_and_checklist_definition(
        tasks_kwargs=[{"label": "Dummy Label"}],
    )
    checklist_task_definition = task_definitions[0]
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    run = Run.create_run_for_team(
        recurrence.team, recurrence.checklist_definition, recurrence
    )
    tasks = ChecklistDefinitionViewSet.related_checklist_tasks(
        checklist_definition, [run], team.members.all()
    )
    run_report = TemplateRunsCsvReport(instance=tasks, is_many=True)

    csv_output = StringIO()
    run_report.write_csv(csv_output)
    csv_output.seek(0)
    # header and 2 data row of 2 checklist of 2 users in the team
    assert len(csv_output.readlines()) == 3

    # Make sure inactive task definition is not contained in CSV report
    checklist_task_definition.active = False
    checklist_task_definition.save()

    tasks = ChecklistDefinitionViewSet.related_checklist_tasks(
        checklist_definition, [run], team.members.all()
    )
    new_run_report = TemplateRunsCsvReport(
        instance=tasks,
        is_many=True,
    )
    assert checklist_task_definition.label not in new_run_report.get_csv_fields()


def test_recurrence_trends_csv_report():
    """
    Test generating CSV report of Recurrence Task Trends
    """
    # Setup
    team, checklist_definition, task_definitions = create_team_and_checklist_definition(
        tasks_kwargs=[
            {"label": "Dummy Label"},
            {"label": "Dummy Label 2"},
        ]
    )
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    recurrence.save()
    for _ in range(2):
        Run.create_run_for_team(
            recurrence.team, recurrence.checklist_definition, recurrence
        )
    runs = Run.objects.all()
    context = {"runs": runs, "users": team.members.values_list("id", flat=True)}

    trends_report = ChecklistDefinitionTrendsCsvReport(
        instance=checklist_definition, context=context
    )

    csv_output = StringIO()
    trends_report.write_csv(csv_output)
    csv_output.seek(0)
    # header + 2 row for 2 task definitions
    assert len(csv_output.readlines()) == 3

    # Make sure inactive task definition is not contained in CSV report
    task_definitions[0].active = False
    task_definitions[0].save()

    csv_output = StringIO()
    trends_report.write_csv(csv_output)
    csv_output.seek(0)

    # header + 2 row for 1 task definitions
    assert len(csv_output.readlines()) == 2

    task_definitions[1].active = False
    task_definitions[1].save()

    csv_output = StringIO()
    trends_report.write_csv(csv_output)
    csv_output.seek(0)

    # header only
    assert len(csv_output.readlines()) == 1


def test_recurrence_run_pdf_task_table():
    """Tests for task table used for renderring PDF"""
    # Setup
    team_member_count: int = 6
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(team_member_count=team_member_count)
    markdown_task_definition = create_checklist_task_definition(
        checklist_definition, MARKDOWN_TEXTAREA_INTERFACE
    )
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    run = Run.create_run_for_team(team, checklist_definition, recurrence)
    markdown_tasks = ChecklistTask.objects.filter(
        definition=markdown_task_definition
    ).all()
    for i in range(3):
        task = markdown_tasks[i]
        task.response = {"content": "Dummy content"}
        task.completed = True
        task.save()

    tasks = ChecklistDefinitionViewSet.related_checklist_tasks(
        checklist_definition, [run], team.members.all()
    )
    pdf_report = TemplateRunsPdfReport(instance=tasks, is_many=True)
    data = pdf_report.get_json_data()
    definition_id_map: dict = TemplateRunsPdfReport.group_tasks_by_definition_id(data)

    # Action
    # Markdown tasks table
    tasks_table_items: list = TemplateRunsPdfReport.get_tasks_table(
        definition_id_map[markdown_task_definition.id]["tasks"],
        definition_id_map[markdown_task_definition.id]["dates"],
    )
    assert len(tasks_table_items) == team_member_count + 1


def test_parse_response_rates_pdf_trends_report():
    """Test parsing response rates for PDF trends report"""
    # pylint: disable=too-many-locals
    team_member_count: int = 6
    default_customization_args = get_interfaces_customization_arg_default_value()
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(
        team_member_count=team_member_count,
        tasks_kwargs=[
            {
                "label": "Test Checkbox",
                "customization_args": default_customization_args[CHECKBOX_INTERFACE],
            }
        ],
    )
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    for _ in range(2):
        Run.create_run_for_team(team, checklist_definition, recurrence)
    runs = Run.objects.all()
    context = {"runs": runs, "users": team.members.values_list("id", flat=True)}

    dt_new = datetime.strptime("2022-06-01T00:00:00.000Z", "%Y-%m-%dT%H:%M:%S.%fZ")
    dt_old = datetime.strptime("2022-06-01T00:00:00Z", "%Y-%m-%dT%H:%M:%SZ")

    runs[0].start_date = dt_old
    runs[0].save()
    runs[1].start_date = dt_new
    runs[1].save()

    serialized_data = ChecklistDefinitionTrendsSerializer(
        instance=checklist_definition,
        context=context,
    ).data
    timestamps, percentages = ChecklistDefinitionTrendsPdfReport.parse_response_rates(
        serialized_data["response_rates"]
    )
    assert len(timestamps) == 2
    assert len(percentages) == 2
    for percentage in percentages:
        assert percentage == 0.0

    checklists = Checklist.objects.all()
    for checklist in checklists:
        checklist.completed = True
        checklist.save()

    serialized_data = ChecklistDefinitionTrendsSerializer(
        instance=checklist_definition,
        context=context,
    ).data
    timestamps, percentages = ChecklistDefinitionTrendsPdfReport.parse_response_rates(
        serialized_data["response_rates"]
    )
    assert len(timestamps) == 2
    assert len(percentages) == 2
    for percentage in percentages:
        assert percentage == 100.0


def test_checkbox_recurrence_trends_pdf_report():
    """Test generating PDF report for recurrence trends of Checkbox task definition"""
    # Setup
    team_member_count: int = 6
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(team_member_count=team_member_count)
    checkbox_task_definition = create_checklist_task_definition(
        checklist_definition, CHECKBOX_INTERFACE
    )
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    run = Run.create_run_for_team(team, checklist_definition, recurrence)
    RunTrendTaskReport.compute_task_trends(checkbox_task_definition, run)

    # Action
    # Checkbox
    RunTrendTaskReport.objects.get(task_definition=checkbox_task_definition, run=run)
    checkbox_report_obj = ChecklistDefinitionTrendsSerializer(
        instance=checklist_definition,
        context={"runs": [run], "users": team.members.values_list("id", flat=True)},
    ).data["trends"][0]
    checkbox_trends_content = (
        ChecklistDefinitionTrendsPdfReport.get_checkbox_trends_content(
            checkbox_report_obj
        )
    )
    assert len(checkbox_trends_content) == 1


def test_linear_scale_rating_recurrence_trends_pdf_report():
    """Test generating PDF report for recurrence trends of
    Linear Scale Rating task definition
    """
    # Setup
    team_member_count: int = 6
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(
        team_member_count=team_member_count, tasks_kwargs=[]
    )
    rating_task_definition = create_checklist_task_definition(
        checklist_definition, LINEAR_SCALE_RATING_INTERFACE
    )
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    run = Run.create_run_for_team(team, checklist_definition, recurrence)

    def get_linear_scale_chart_content():
        RunTrendTaskReport.compute_task_trends(rating_task_definition, run)
        linear_scale_report_obj = ChecklistDefinitionTrendsSerializer(
            instance=checklist_definition,
            context={"runs": [run], "users": team.members.values_list("id", flat=True)},
        ).data["trends"][0]
        linear_scale_trends_content = (
            ChecklistDefinitionTrendsPdfReport.get_linear_scale_trends_content(
                linear_scale_report_obj
            )
        )
        linear_scale_chart_image = (
            ChecklistDefinitionTrendsPdfReport.get_linear_scale_chart_image(
                linear_scale_report_obj["interface_stats"],
                linear_scale_report_obj["task_definition"]["customization_args"],
                linear_scale_report_obj["response_rate"]["completed_count"],
            )
        )
        return linear_scale_trends_content, linear_scale_chart_image

    (
        linear_scale_trends_content,
        linear_scale_chart_image,
    ) = get_linear_scale_chart_content()
    # Mean, median and mode and a None object
    assert len(linear_scale_trends_content) == 4
    assert linear_scale_chart_image is None

    # Complete some tasks and recompute
    tasks = ChecklistTask.objects.filter(definition=rating_task_definition).all()
    for task in tasks:
        task.response = {"rating": 3}
        task.completed = True
        task.save()
    (
        linear_scale_trends_content,
        linear_scale_chart_image,
    ) = get_linear_scale_chart_content()
    # Mean, median and mode and a chart image
    assert linear_scale_chart_image is not None
    assert len(linear_scale_trends_content) == 4


def test_markdown_recurrence_trends_pdf_report():
    """Test generating PDF report for recurrence trends of markdown task definition"""
    # Setup
    team_member_count: int = 6
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(
        team_member_count=team_member_count,
        tasks_kwargs=[],
    )
    markdown_task_definition = create_checklist_task_definition(
        checklist_definition, MARKDOWN_TEXTAREA_INTERFACE
    )
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    run = Run.create_run_for_team(team, checklist_definition, recurrence)
    RunTrendTaskReport.compute_task_trends(markdown_task_definition, run)

    # Action
    markdown_report_obj = ChecklistDefinitionTrendsSerializer(
        instance=checklist_definition,
        context={"runs": [run], "users": team.members.values_list("id", flat=True)},
    ).data["trends"][0]
    markdown_trends_content = (
        ChecklistDefinitionTrendsPdfReport.get_markdown_trends_content(
            markdown_report_obj
        )
    )
    assert len(markdown_trends_content) == 1


def test_numeric_recurrence_trends_pdf_report():
    """Test generating PDF report for recurrence trends of Numeric task definition"""
    # Setup
    team_member_count: int = 6
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(
        team_member_count=team_member_count, tasks_kwargs=[]
    )
    numeric_task_definition = create_checklist_task_definition(
        checklist_definition, NUMERIC_INTERFACE
    )
    recurrence = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    run = Run.create_run_for_team(team, checklist_definition, recurrence)
    RunTrendTaskReport.compute_task_trends(numeric_task_definition, run)

    # Action
    numeric_report_obj = ChecklistDefinitionTrendsSerializer(
        instance=checklist_definition,
        context={"runs": [run], "users": team.members.values_list("id", flat=True)},
    ).data["trends"][0]
    numeric_trends_content = (
        ChecklistDefinitionTrendsPdfReport.get_numeric_trends_content(
            numeric_report_obj
        )
    )
    # Only mean, median and mode
    assert len(numeric_trends_content) == 3
