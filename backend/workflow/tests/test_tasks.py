"""
Tests for Workflow models.
"""
from unittest.mock import patch

import pytest
from django.core import mail
from django.utils import timezone
from django_celery_beat.models import DAYS, IntervalSchedule
from factory import CREATE_STRATEGY
from profiles.tests.factories import TeamFactory, UserFactory
from workflow.models import Recurrence, Run, Checklist
from workflow.tasks import (
    compute_task_definition_trends_report,
    create_checklist_for_users,
    send_reminder_emails,
)
from workflow.tests.factories import (
    ChecklistDefinitionFactory,
    ChecklistTaskDefinitionFactory,
    EmailFactory,
    RecurrenceFactory,
)

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("celery_worker", "patch_flush_many_cache"),
]


@patch("workflow.models.Run.create_run_for_team")
def test_create_checklist_for_users_calls_create_run_for_team(mock_verify):
    """Test create_checklist_for_users method calls Run.create_run_for_team."""
    # Basline verification.
    assert not mock_verify.called

    # Setup.
    team = TeamFactory()
    team.members.set([UserFactory()])
    checklist_definition = ChecklistDefinitionFactory()
    recurrence_instance = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )

    # Action.
    create_checklist_for_users(recurrence_instance.id)

    # End verification.
    assert mock_verify.called


def test_create_checklist_for_users_sends_no_email():
    """Tests that no emails are sent out on checklist creation."""

    # Setup.
    user_1 = UserFactory()
    user_2 = UserFactory()
    team = TeamFactory()
    team.members.set([user_1, user_2])
    checklist_definition = ChecklistDefinitionFactory()
    recurrence_instance = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )

    # Action.
    create_checklist_for_users(recurrence_instance.id)

    # End verification.
    assert len(mail.outbox) == 0


@patch("workflow.models.RunTrendTaskReport.compute_task_trends")
def test_compute_task_definition_trends_report(mock_verify):
    """
    Check if task trend report is updated when task is saved
    """
    # Basline verification.
    assert not mock_verify.called

    # Setup.
    team = TeamFactory()
    team.members.set([UserFactory()])
    checklist_definition = ChecklistDefinitionFactory()
    definition = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition
    )
    RecurrenceFactory.create(team=team, checklist_definition=checklist_definition)

    compute_task_definition_trends_report(definition.id)

    # End verification.
    assert mock_verify.called


@pytest.mark.parametrize("reminders,email_type", [[1, "Reminder"], [3, "Please"]])
def test_send_reminder_emails_default(reminders, email_type):
    """Test send_reminder_emails sends email to team members."""
    # Basline verification.
    assert len(mail.outbox) == 0

    # Setup.
    user_1 = UserFactory()
    user_2 = UserFactory()
    team = TeamFactory()
    team.members.set([user_1, user_2])
    checklist_task_definition = ChecklistTaskDefinitionFactory(
        checklist_definition__name="Test & Check"
    )
    checklist_definition = checklist_task_definition.checklist_definition
    interval_schedule = IntervalSchedule.objects.create(every=5, period=DAYS)
    recurrence_instance = Recurrence.objects.create(
        team=team,
        checklist_definition=checklist_definition,
        interval_schedule=interval_schedule,
        due_days_no=2,
        reminders=[reminders],
    )
    Run.create_run_for_team(team, checklist_definition, recurrence_instance)

    # reminder_date should be reminder_days before due_date
    reminder_date = Run.objects.first().due_date - timezone.timedelta(
        recurrence_instance.reminders[0]
    )
    # Action.
    send_reminder_emails(reminder_date.strftime("%Y-%m-%d"))

    # End verification.
    assert len(mail.outbox) == 2
    emails_to = [mail.to[0] for mail in mail.outbox]
    email_subject = mail.outbox[0].subject
    assert user_1.email in emails_to
    assert user_2.email in emails_to
    assert checklist_definition.name in email_subject
    assert email_type in email_subject


@pytest.mark.parametrize("reminders,instance_type", [[1, 1], [3, 0]])
def test_send_reminder_emails_database(reminders, instance_type):
    """Test send_reminder_emails sends email to team members."""
    # Basline verification.
    assert len(mail.outbox) == 0

    # Setup.
    user_1 = UserFactory()
    user_2 = UserFactory()
    team = TeamFactory()
    team.members.set([user_1, user_2])
    checklist_task_definition = ChecklistTaskDefinitionFactory()
    checklist_definition = checklist_task_definition.checklist_definition
    interval_schedule = IntervalSchedule.objects.create(every=5, period=DAYS)
    email_instances = EmailFactory.generate_batch(
        strategy=CREATE_STRATEGY,
        size=2,
        reply_to=["test@test.com"],
        subject="{{ checklist_name }}",
    )
    recurrence_instance = Recurrence.objects.create(
        team=team,
        checklist_definition=checklist_definition,
        interval_schedule=interval_schedule,
        due_days_no=2,
        reminders=[reminders],
        notification_email=email_instances[0],
        reminder_email=email_instances[1],
    )
    Run.create_run_for_team(team, checklist_definition, recurrence_instance)

    # reminder_date should be reminder_days before due_date
    reminder_date = Run.objects.first().due_date - timezone.timedelta(
        recurrence_instance.reminders[0]
    )
    # Action.
    send_reminder_emails(reminder_date.strftime("%Y-%m-%d"))

    # End verification.
    assert len(mail.outbox) == 2
    emails_to = [mail.to[0] for mail in mail.outbox]
    assert user_1.email in emails_to
    assert user_2.email in emails_to
    email_subject = mail.outbox[0].subject
    assert checklist_definition.name == email_subject
    assert email_instances[instance_type].text_body == mail.outbox[0].body
    assert email_instances[instance_type].html_body == mail.outbox[0].alternatives[0][0]
    assert email_instances[instance_type].reply_to == mail.outbox[0].reply_to


def test_send_reminder_emails_skips_completed():
    """Test send_reminder_emails skips completed checklists."""
    # Basline verification.
    assert len(mail.outbox) == 0

    # Setup.
    user_1 = UserFactory()
    user_2 = UserFactory()
    team = TeamFactory()
    team.members.set([user_1, user_2])
    checklist_task_definition = ChecklistTaskDefinitionFactory()
    checklist_definition = checklist_task_definition.checklist_definition
    interval_schedule = IntervalSchedule.objects.create(every=1, period=DAYS)
    recurrence_instance = Recurrence.objects.create(
        team=team,
        checklist_definition=checklist_definition,
        interval_schedule=interval_schedule,
    )
    Run.create_run_for_team(team, checklist_definition, recurrence_instance)
    Checklist.objects.filter(assignee=user_2).update(completed=True)

    # Action.
    send_reminder_emails()

    # End verification.
    assert len(mail.outbox) == 1
    email_to = mail.outbox[0].to[0]
    email_subject = mail.outbox[0].subject
    assert user_1.email == email_to
    assert checklist_definition.name in email_subject
