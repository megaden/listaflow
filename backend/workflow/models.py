"""
Database models for the workflow module.
"""
from collections import defaultdict
import json
import calendar

from typing import Tuple, Optional, List
from django.conf import settings
from django.db import models
from django.db.models import F, Q

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django_better_admin_arrayfield.models.fields import ArrayField
from django_celery_beat.models import (
    PeriodicTask,
    IntervalSchedule,
)

from short_stuff import gen_shortcode
from short_stuff.django.models import ShortCodeField
from taggit.managers import TaggableManager

from lib.models import DateTrackedModel, TaggedItemBase
from profiles.models import Team, TeamMembership
from utils.exceptions import raise_django_validation_error
from workflow.interaction import (
    CHECKBOX_INTERFACE,
    INTERFACE_TYPE_CHOICES,
    INTERFACE_TYPE_TO_CLASS,
    response_repr,
)
from workflow.mixins import BaseReminderModel
from workflow.managers import ChecklistManager
from workflow.enums import ChecklistStatus
from workflow.report.trends_builder import TaskResponseRate


class ChecklistDefinition(DateTrackedModel):
    """
    Checklist Definition. Collection of task definitions and settings from which to
    template out a checklist instance.
    """

    def active_task_definitions(self):
        """
        Gets a queryset of all active (that is, not deleted) task definitions on this
        checklist definition.
        """
        return self.task_definitions.filter(active=True)

    id = ShortCodeField(primary_key=True, db_index=True, default=gen_shortcode)
    # We don't delete definitions that are attached to existing checklists.
    active = models.BooleanField(default=True, db_index=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    body = models.TextField(max_length=5000, default="", blank=True)

    def __str__(self) -> str:
        return self.name

    def related_runs(self):
        """
        fetch all runs for checklist definition
        """
        runs = Run.objects.filter(checklists__in=self.checklists.all())
        return runs.distinct()

    def related_tags(self):
        """
        Fetch related tags for this ChecklistDefinition.
        """
        tags = (
            TaggedTaskDefinition.objects.filter(
                content_object__in=self.active_task_definitions().values_list(
                    "id", flat=True
                )
            )
            .annotate(name=F("tag__name"))
            .values("name", "id")
            .distinct("name")
        )
        return tags

    # pylint: disable=no-self-use
    def get_response_rates(self, runs, users):
        """
        Get response rates from runs
        """
        response_rates: list[dict] = []
        for run in runs:
            response_rate: dict = {}
            response_rate["timestamp"] = run.start_date
            response_rate["completed_count"] = run.checklists.filter(
                assignee__in=users, completed=True
            ).count()
            response_rate["total_count"] = run.checklists.filter(
                assignee__in=users
            ).count()
            response_rates.append(response_rate)
        return response_rates

    # pylint: disable=no-self-use
    def all_tasks_trends(self, runs, users):
        """
        Get tasks trends from runs
        """
        task_reports = defaultdict(dict)
        for run in runs:
            run_trend = run.tasks_trends.filter(
                task_definition__active=True,
                task_definition__tasks__checklist__assignee__in=users,
            ).distinct()
            for trend in run_trend:
                task_report = task_reports[trend.task_definition.id]
                task_report["task_definition"] = trend.task_definition
                if "response_rate" not in task_report:
                    task_report["response_rate"] = TaskResponseRate()
                required_user_completion_map = {
                    k: v
                    for k, v in trend.user_completion_map.items()
                    if int(k) in users
                }
                completed_count = sum(
                    1
                    for completed in required_user_completion_map.values()
                    if completed
                )
                task_report["response_rate"].register(
                    completed_count,
                    len(required_user_completion_map.values()),
                )
                interface_cls: type = INTERFACE_TYPE_TO_CLASS[
                    trend.task_definition.interface_type
                ]
                if "interface_stats" not in task_report:
                    task_report[
                        "interface_stats"
                    ] = interface_cls.combine_stats_builder()
                if task_report["interface_stats"] is not None:
                    task_report["interface_stats"].register(
                        trend.interface_stats, completed_count, users
                    )
        return list(task_reports.values())


class TaggedTaskDefinition(TaggedItemBase):
    """
    Custom tagged through model, in order to reduce lookup count in
    already-dense queries. Otherwise, it would use a GenericForeignKey.
    """

    content_object = models.ForeignKey(
        "ChecklistTaskDefinition", on_delete=models.CASCADE
    )


class TaggedTask(TaggedItemBase):
    """
    Custom tagged through model, in order to reduce lookup count in
    already-dense queries. Otherwise, it would use a GenericForeignKey.
    """

    content_object = models.ForeignKey("ChecklistTask", on_delete=models.CASCADE)


class ChecklistTaskDefinition(DateTrackedModel):
    """
    ChecklistTask Definition model. Used to define the information on a task.
    """

    id = ShortCodeField(primary_key=True, db_index=True, default=gen_shortcode)
    # We don't delete definitions that are attached to existing tasks.
    active = models.BooleanField(default=True, db_index=True)
    required = models.BooleanField(default=True, db_index=True)
    label = models.CharField(max_length=200)
    body = models.TextField(max_length=5000, default="", blank=True)
    checklist_definition = models.ForeignKey(
        ChecklistDefinition,
        on_delete=models.CASCADE,
        related_name="task_definitions",
    )
    interface_type = models.CharField(
        choices=INTERFACE_TYPE_CHOICES, default=CHECKBOX_INTERFACE, max_length=50
    )
    customization_args = models.JSONField(default=dict, blank=True)
    tags = TaggableManager(
        through=TaggedTaskDefinition,
        blank=True,
        help_text=_(
            "Only team members who have these tags will get this task on a "
            "recurrence-generated run."
        ),
    )

    @property
    def author(self):
        """
        Gets the author of this checklist task. Useful in permissions checks.
        """
        return self.checklist_definition.author

    class Meta:
        ordering = ("created_on",)

    def __str__(self) -> str:
        return self.label

    def clean(self) -> None:
        interface_class = INTERFACE_TYPE_TO_CLASS[self.interface_type]
        interface_class.validate_customization_args(
            self.customization_args, required=self.required
        )
        return super().clean()

    def save(self, *args, **kwargs) -> None:
        self.clean()
        super().save(*args, **kwargs)


class Checklist(models.Model):
    """
    A checklist assigned to someone to run through.
    """

    id = ShortCodeField(primary_key=True, db_index=True, default=gen_shortcode)
    assignee = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    # Always make sure this is longer than the definition's name field, so
    # suffixes/prefixes can be added.
    name = models.CharField(max_length=250, db_index=True)
    definition = models.ForeignKey(
        ChecklistDefinition, on_delete=models.CASCADE, related_name="checklists"
    )
    completed = models.BooleanField(default=False, db_index=True)
    # Not using the DateTrackedModel here since we don't want updated_on to be depended
    # on for the sake of checking when the last subtask was handled.
    created_on = models.DateTimeField(db_index=True, default=timezone.now)
    completed_on = models.DateTimeField(db_index=True, null=True, blank=True)
    started_on = models.DateTimeField(db_index=True, null=True, blank=True)
    # In most cases, these three date fields will be synchronized with the Run's
    # settings. They're here to improve query performance.
    start_date = models.DateTimeField(
        blank=True,
        null=True,
        db_index=True,
        verbose_name=_("Start Datetime"),
    )
    end_date = models.DateTimeField(
        blank=True,
        null=True,
        db_index=True,
        verbose_name=_("End Datetime"),
    )
    due_date = models.DateField(
        blank=True,
        null=True,
        db_index=True,
        verbose_name=_("Due date"),
    )
    is_archived = models.BooleanField(default=False, db_index=True)
    run = models.ForeignKey(
        "Run",
        default=None,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="checklists",
    )
    objects = ChecklistManager()

    @property
    def body(self):
        """
        Get the body from the definition.
        """
        return self.definition.body

    @property
    def completed_tasks(self):
        """
        Get completed tasks
        """
        return self.active_tasks().filter(completed=True)

    class Meta:
        ordering = ("-created_on",)

    def __str__(self) -> str:
        return self.name

    def save(self, *args, **kwargs) -> None:
        if self.completed and not self.completed_on:
            self.completed_on = timezone.now()
        elif not self.completed:
            self.completed_on = None
        super().save(*args, **kwargs)

    def complete(self) -> Tuple[bool, int]:
        """Save, updating the checklist's status."""
        pending_tasks = self.tasks.filter(
            completed=False, definition__required=True
        ).count()
        if pending_tasks:
            return self.completed, pending_tasks
        self.completed = True
        self.save()
        return self.completed, pending_tasks

    def active_tasks(self):
        """
        Return queryset for active tasks only
        """
        return self.tasks.filter(definition__active=True)

    @property
    def status(self):
        """
        Status of checklist based on statuses of tasks
        """
        if self.completed:
            return ChecklistStatus.COMPLETED
        if not self.completed and self.completed_tasks.count() > 0:
            return ChecklistStatus.IN_PROGRESS
        today = timezone.now().date()
        if (
            not self.completed
            and self.run is not None
            and self.run.due_date
            and self.run.end_date
        ):
            if self.run.due_date < today:
                return ChecklistStatus.PAST_DUE
            if self.run.due_date > self.run.end_date.date() > today:
                return ChecklistStatus.UP_COMING
        return ChecklistStatus.TO_DO

    def task_names_list(self):
        """
        Returns list of task names in this checklist
        """
        tasks = self.active_tasks().values_list("definition__label", flat=True)
        return tasks

    @classmethod
    def create_checklist_and_corresponding_tasks(
        cls,
        assignee,
        checklist_definition_instance,
        user_tags=None,
    ) -> Tuple[Optional["Checklist"], List["ChecklistTask"]]:
        """Creates a Checklist and Task instances for the given
        ChecklistDefinition instance.

        Args:
            assignee: User. The user to assign to the checklist and task instance.
            checklist_definition_instance: ChecklistDefinition. The definition
                instance for creating the checklist.
            tags: Queryset of Tags. If a TaskDefinition has tags, it will only create
            the task if the tag queryset contains a matching tag.

        Returns:
            Checklist, list(Task). The newly created Checklist and Task instances.
        """
        tasks = []
        tags = user_tags or []
        tag_filter = Q(tags__isnull=True)
        if tags:
            tag_filter |= Q(tags__in=tags)
        definitions = checklist_definition_instance.task_definitions.filter(
            active=True
        ).filter(tag_filter)
        if not definitions.exists():
            return None, []
        instance = cls.objects.create(
            name=checklist_definition_instance.name,
            assignee=assignee,
            definition=checklist_definition_instance,
        )
        for task_definition in definitions:
            task_instance = ChecklistTask.objects.create(
                definition=task_definition,
                checklist=instance,
            )
            task_instance.tags.set(task_definition.tags.all())
            tasks.append(task_instance)

        return instance, tasks


class Task(models.Model):
    """
    Abstract model for tasks. May be used for tasks in different contexts.
    """

    id = ShortCodeField(primary_key=True, db_index=True, default=gen_shortcode)
    completed = models.BooleanField(default=False, db_index=True)
    completed_on = models.DateTimeField(null=True, db_index=True, blank=True)

    def save(self, *args, **kwargs) -> None:
        if self.completed and not self.completed_on:
            self.completed_on = timezone.now()
        elif not self.completed:
            self.completed_on = None
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class ChecklistTask(Task):
    """
    ChecklistTask object for a user to check off.
    """

    id = ShortCodeField(primary_key=True, db_index=True, default=gen_shortcode)
    definition = models.ForeignKey(
        ChecklistTaskDefinition, on_delete=models.CASCADE, related_name="tasks"
    )
    checklist = models.ForeignKey(
        Checklist, on_delete=models.CASCADE, related_name="tasks"
    )
    response = models.JSONField(default=None, blank=True, null=True)
    tags = TaggableManager(
        through=TaggedTask,
        blank=True,
        help_text=_(
            "Only team members who have these tags will get this task on a "
            "recurrence-generated run."
        ),
    )

    @property
    def label(self):
        """
        Get the label from the definition.
        :return:
        """
        return self.definition.label

    @property
    def body(self):
        """
        Get the body from the definition.
        """
        return self.definition.body

    @property
    def required(self):
        """
        Get required field from the definition.
        """
        return self.definition.required

    @property
    def assignee(self):
        """
        Get assignee from the checklist.
        """
        return self.checklist.assignee

    @property
    def interface_type(self):
        """
        Get interface_type field from the definition.
        """
        return self.definition.interface_type

    @property
    def customization_args(self):
        """
        Get customization_args field from the definition.
        """
        return self.definition.customization_args

    @property
    def response_display(self):
        """
        Display of response with different interfaces
        """
        return response_repr(
            self.response,
            self.definition.interface_type,
            self.definition.customization_args,
        )

    def save(self, *args, **kwargs) -> None:
        """Save, updating the checklist's status."""
        super().save(*args, **kwargs)
        if self.required and not self.completed:
            self.checklist.completed = False
        if self.completed:
            self.checklist.started_on = self.checklist.started_on or timezone.now()
        self.checklist.save()

    class Meta:
        ordering = ("definition__created_on",)

    def __str__(self) -> str:
        return self.label


class Email(models.Model):
    """Email model to store templates, subject and reply-to address for a
    recurrence"""

    subject = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        verbose_name=_("Email Subject"),
        help_text=_("Template tags like {{ checklist_name }} can be included."),
    )
    text_body = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Email text body"),
        help_text=_("Template tags like {{ checklist_name }} can be included."),
    )
    html_body = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Email html body"),
        help_text=_("Template tags like {{ checklist_name }} can be included."),
    )
    reply_to = ArrayField(
        models.EmailField(),
        null=True,
        blank=True,
        verbose_name=_("Email reply-to addresses"),
        help_text=_("Replies to this email will be sent to these addresses"),
    )

    def __str__(self):
        return self.subject


class Recurrence(BaseReminderModel):
    """The Recurrence model to store recurring checklists/tasks for a team."""

    team = models.ForeignKey(
        Team, on_delete=models.CASCADE, related_name="recurrent_checklists"
    )
    start_date = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name=_("Start Datetime"),
        help_text=_(
            "Datetime when the schedule should begin " "triggering the task to run"
        ),
    )
    interval_schedule = models.ForeignKey(
        IntervalSchedule,
        on_delete=models.CASCADE,
        null=True,
        blank=False,
        verbose_name=_("Interval Schedule"),
        help_text=_("Interval Schedule to run the task on."),
    )
    periodic_task = models.OneToOneField(
        PeriodicTask, on_delete=models.CASCADE, default=None
    )
    active = models.BooleanField(default=True, db_index=True)
    checklist_definition = models.ForeignKey(
        ChecklistDefinition, on_delete=models.CASCADE, related_name="recurrences"
    )
    due_days_no = models.PositiveIntegerField(
        blank=True,
        null=True,
        default=0,
        help_text="Days after end date the due date will be",
    )
    notification_email = models.ForeignKey(
        Email,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("Notification email template"),
        help_text=_("Sent when more than 1 day is left before due date"),
        related_name="notified_recurrences",
    )
    reminder_email = models.ForeignKey(
        Email,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("Reminder email template"),
        help_text=_("Sent when only 1 day is left before due date"),
        related_name="reminded_recurrences",
    )

    @property
    def recurring_schedule_display(self):
        """Recurring schedule display for recurrence"""
        period = self.interval_schedule.period
        every = self.interval_schedule.every
        if period == IntervalSchedule.DAYS and every % 7 == 0:
            every_week_count = every // 7
            day_name = calendar.day_name[self.start_date.weekday()]
            weekday = _(day_name)
            if every_week_count == 1:
                return weekday
            return _("Every {every_week_count} {weekday}").format(
                every_week_count=every_week_count, weekday=weekday
            )
        return _("Every {every} {period}").format(every=every, period=period)

    def clean(self) -> None:
        """Validates start_date and interval_schedule.
        Raises error if start_date or interval_schedule is updated and
        start_date is in past
        """
        is_in_past = (
            self.start_date
            and timezone.now() - self.start_date > timezone.timedelta(minutes=5)
        )
        if hasattr(self, "periodic_task"):
            if (
                self.interval_schedule
                and self.periodic_task.interval != self.interval_schedule
                or self.periodic_task.start_time != self.start_date
            ) and is_in_past:
                raise_django_validation_error(
                    "start_date", _("Start date cannot be set in past")
                )
        else:
            if is_in_past:
                raise_django_validation_error(
                    "start_date", _("Start date cannot be set in past")
                )
        return super().clean()

    def _get_last_run(self):
        """Start interval scheduled tasks exactly at given start time django
        celery beat starts the task at start_time + interval time by default
        Setting last_run_at to a start_time - interval makes sure that the
        task starts at start_time
        https://github.com/celery/django-celery-beat/issues/259#issuecomment-516425854
        """
        if self.interval_schedule is None or self.start_date is None:
            return None
        now = timezone.now()
        remaining_estimate = self.interval_schedule.schedule.remaining_estimate(now)
        return self.start_date - remaining_estimate

    def create_or_update_periodic_task(self):
        """
        Updates periodic_task if already exists with
        correct last_run_at
        """
        updated_values = {
            "interval": self.interval_schedule,
            "start_time": self.start_date,
            "enabled": self.active,
        }
        prev_task = getattr(self, "periodic_task", None)

        if prev_task is None:
            updated_values["last_run_at"] = self._get_last_run()
        else:
            schedule_changed = (
                self.periodic_task.interval != self.interval_schedule
                or self.periodic_task.start_time != self.start_date
            )
            enabled_for_future = (
                self.active
                and not self.periodic_task.enabled
                and self.start_date > timezone.now()
            )

            if schedule_changed or enabled_for_future:
                updated_values["last_run_at"] = self._get_last_run()

        self.periodic_task, created = PeriodicTask.objects.get_or_create(
            defaults=updated_values,
            name=f"Period task for Reccurence model with ID {self.id}, interval",
            task="workflow.tasks.create_checklist_for_users",
            args=json.dumps([self.id]),
        )

        # if schedule_type changed, disbale the previous periodic_task
        if prev_task and prev_task.id != self.periodic_task.id:
            prev_task.enabled = False
            prev_task.save()

        # update periodic_task if exists
        if not created:
            self.periodic_task.__dict__.update(**updated_values)
            self.periodic_task.interval = self.interval_schedule
            self.periodic_task.save()

    def save(self, *args, **kwargs):
        """Saves the updated model value into datastore.

        Note: This method ensures creating and linking PeriodicTask model in
        Recurrence model. The PeriodicTask model is used by celery to run tasks
        periodically.
        """
        self.validate_unique()
        self.clean()
        self.create_or_update_periodic_task()
        super().save(*args, **kwargs)

    def __str__(self):
        return (
            f"Recurring checklist '{self.checklist_definition.name}' "
            f"for Team({self.team.name})"
        )


class Run(BaseReminderModel):
    """The Run model to store checklists assigned to a team.

    Attributes:
        start_date: DateField. The starting date of the assigned checklists.
        end_date: DateField.The ending date of the assigned checklists.
        team: Team. The team corresponds to the given Run.
        checklists. list(Checklist). The Checklists assigned to member of the team.
    """

    start_date = models.DateTimeField(
        blank=True,
        null=True,
        db_index=True,
        verbose_name=_("Start Datetime"),
    )
    end_date = models.DateTimeField(
        blank=True,
        null=True,
        db_index=True,
        verbose_name=_("End Datetime"),
    )
    due_date = models.DateField(
        blank=True,
        null=True,
        db_index=True,
        verbose_name=_("Due date"),
    )
    team = models.ForeignKey(
        Team, on_delete=models.CASCADE, related_name="recurrent_runs"
    )
    recurrence = models.ForeignKey(
        Recurrence,
        on_delete=models.SET_NULL,
        related_name="runs",
        null=True,
        blank=True,
    )

    @property
    def checklist_definition(self):
        """
        Assuming only single type of checklists are added
        returns the checklist definition
        """
        # pylint: disable=no-member
        checklist = self.checklists.first()
        if checklist:
            return checklist.definition
        return None

    @property
    def team_name(self):
        """
        Get team name for the run
        """
        return self.team.name

    class Meta:
        ordering = ("-start_date",)

    def clean(self) -> None:
        if not self.due_date and self.end_date:
            self.due_date = self.end_date.date()
        return super().clean()

    def save(self, *args, **kwargs):
        self.clean()
        self.checklists.update(
            start_date=self.start_date,
            end_date=self.end_date,
            due_date=self.due_date,
        )
        return super().save(*args, **kwargs)

    @classmethod
    def create_run_for_team(
        cls, team_instance, checklist_definition_instance, recurrence_instance=None
    ):
        """Creates Run model for the given checklist definition for a team.

        Args:
            team_instance: Team. The team model for creating the run.
            checklist_definition_instance: ChecklistDefinition. The given
                checklist definition model.
            recurrence_instance: Recurrence. This is passed in case a
            run is created from Recurrence instance.
        """
        # pylint: disable=import-outside-toplevel,cyclic-import
        from workflow.signals import post_recurrence_run_initialized

        checklists = []
        now = timezone.now()
        run_data = {
            "team": team_instance,
            "start_date": now,
            "end_date": now,
            "due_date": now,
            "recurrence": recurrence_instance,
        }
        for membership in TeamMembership.objects.filter(
            team=team_instance, user__is_active=True
        ):
            checklist, _ = Checklist.create_checklist_and_corresponding_tasks(
                membership.user,
                checklist_definition_instance,
                user_tags=membership.tags.all(),
            )
            if checklist:
                checklists.append(checklist)
        if not checklists:
            return None
        if recurrence_instance:
            end_date = run_data[
                "start_date"
            ] + recurrence_instance.periodic_task.schedule.remaining_estimate(
                run_data["start_date"]
            )
            run_data["end_date"] = end_date
            run_data["due_date"] = end_date.date() + timezone.timedelta(
                days=recurrence_instance.due_days_no
            )
            run_data["reminders"] = recurrence_instance.reminders
        instance = cls.objects.create(**run_data)
        instance.checklists.set(checklists)
        # Run all post-save hooks.
        instance.save()
        post_recurrence_run_initialized.send(sender=cls, instance=instance)
        return instance

    @property
    def completed_checklist_count(self):
        """
        Count of completed checklists
        """
        return self.checklists.filter(completed=True).count()

    @property
    def assignees(self):
        """
        List of assignees
        """
        checklists = self.checklists.all()
        return [cl.assignee for cl in checklists]

    @property
    def total_checklist_count(self):
        """
        Count of all checklists
        """
        return self.checklists.count()

    def related_tasks(self):
        """
        Count of completed task
        """
        return ChecklistTask.objects.filter(
            checklist__in=self.checklists.all(), definition__active=True
        )

    def __str__(self) -> str:
        recurrence_str = ""
        if self.recurrence:
            recurrence_str = f" for {self.recurrence.id}"
        return (
            f"Run{recurrence_str}, <checklist definition:"
            f" {self.checklist_definition}>"
        )


class RunTrendTaskReport(DateTrackedModel):
    """
    Store tasks trends of a Run
    """

    id = ShortCodeField(primary_key=True, db_index=True, default=gen_shortcode)
    task_definition = models.ForeignKey(
        ChecklistTaskDefinition, on_delete=models.CASCADE
    )
    run = models.ForeignKey(Run, on_delete=models.CASCADE, related_name="tasks_trends")
    interface_stats = models.JSONField(default=dict, blank=True)
    user_completion_map = models.JSONField(default=dict, blank=True)

    @classmethod
    def compute_task_trends(cls, task_definition, run=None):
        """
        Compute task trend for tasks trends of given task_definition
        and run (if passed)

        Args:
            task_definition: ChecklistTaskDefinition.
            run: Run.
        """
        query_dict = {"task_definition": task_definition}
        if run is not None:
            query_dict["run"] = run
        instances = cls.objects.filter(**query_dict)
        for instance in instances:
            run = instance.run
            task_queryset = (
                run.related_tasks()
                .filter(definition=instance.task_definition)
                .annotate(user=F("checklist__assignee"))
                .values("response", "user", "completed")
            )
            instance.user_completion_map = {
                item["user"]: item["completed"] for item in task_queryset
            }
            interface_cls: type = INTERFACE_TYPE_TO_CLASS[
                task_definition.interface_type
            ]
            interface_stats: dict = interface_cls.accumulate_interface_instances(
                task_queryset
            )
            instance.interface_stats = interface_stats
            instance.save()

    class Meta:
        ordering = ("task_definition__created_on",)
