"""
Django admin module for workflow.
"""

from django.contrib import admin
from django.db import models
from django.forms import Textarea
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin

from workflow.interaction import (
    get_interfaces_customization_arg_default_value,
    get_interfaces_customization_arg_schema,
)
from workflow.models import (
    Checklist,
    ChecklistDefinition,
    ChecklistTask,
    ChecklistTaskDefinition,
    Email,
    Recurrence,
    Run,
    RunTrendTaskReport,
)
from workflow.widgets import SchemaEditorWidget

admin.site.register(ChecklistTask)


class ChecklistTaskDefinitionInline(admin.TabularInline):
    """
    add/edit ChecklistTaskDefinition on the same page as ChecklistDefinition
    """

    model = ChecklistTaskDefinition

    exclude = (
        "created_on",
        "updated_on",
    )
    # SchemaEditorWidget renders forms based on required fields for the selected
    # interface_type.
    formfield_overrides = {
        models.JSONField: {
            "widget": SchemaEditorWidget(
                "interface_type",
                get_interfaces_customization_arg_schema(),
                get_interfaces_customization_arg_default_value(),
            )
        },
        models.TextField: {"widget": Textarea(attrs={"rows": 4, "cols": 40})},
    }


@admin.register(ChecklistDefinition)
class ChecklistDefinitionAdmin(admin.ModelAdmin):
    """
    ChecklistDefinition model admin.
    """

    raw_id_fields = ("author",)
    inlines = [
        ChecklistTaskDefinitionInline,
    ]


@admin.register(ChecklistTaskDefinition)
class ChecklistTaskDefinitionAdmin(admin.ModelAdmin):
    """
    ChecklistTaskDefinition model admin.
    """

    raw_id_fields = ("checklist_definition",)


@admin.register(Email)
class EmailModelAdmin(admin.ModelAdmin, DynamicArrayMixin):
    """
    Email model admin.
    """

    list_display = ("subject", "reply_to")


@admin.register(Recurrence)
class RecurrenceAdmin(admin.ModelAdmin, DynamicArrayMixin):
    """
    Recurrence model admin.
    """

    list_display = (
        "__str__",
        "active",
        "interval_schedule",
        "start_date",
        "due_days_no",
    )
    exclude = ("periodic_task",)
    raw_id_fields = ("team",)

    def get_form(self, request, obj=None, change=False, **kwargs):
        """
        Return a Form class for use in the admin add view. This is used by
        add_view and change_view.
        """
        form = super().get_form(request, obj=None, change=False, **kwargs)

        # remove interval_schedule edit and delete option
        def _disable_edit_delete(field_name):
            field = form.base_fields[field_name]
            field.widget.can_change_related = False
            field.widget.can_delete_related = False

        _disable_edit_delete("interval_schedule")
        _disable_edit_delete("notification_email")
        _disable_edit_delete("reminder_email")
        return form


@admin.register(Run)
class RunAdmin(admin.ModelAdmin, DynamicArrayMixin):
    """
    Recurrence model admin.
    """

    list_display = (
        "__str__",
        "team",
        "start_date",
        "end_date",
        "due_date",
        "recurrence",
    )
    raw_id_fields = ("team",)


@admin.register(Checklist)
class ChecklistAdmin(admin.ModelAdmin):
    """
    Recurrence model admin.
    """

    list_display = ("__str__", "assignee", "completed", "created_on", "status")
    raw_id_fields = ("definition", "assignee", "run")


@admin.register(RunTrendTaskReport)
class RunTrendTaskReportAdmin(admin.ModelAdmin):
    """
    RunTrendTaskReport model admin.
    """

    list_display = ("task_definition", "run")
