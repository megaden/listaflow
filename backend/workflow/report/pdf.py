"""
Handle caching and CSV writing logic for reports
"""
import io
from copy import deepcopy
from datetime import datetime

from django.utils.dateparse import parse_datetime

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
from reportlab.graphics.shapes import Drawing, Rect, String
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.fonts import tt2ps
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.platypus import Image, Paragraph, SimpleDocTemplate, Table, TableStyle
from reportlab.rl_settings import canvas_basefontname

from workflow.interaction import (
    CHECKBOX_INTERFACE,
    LINEAR_SCALE_RATING_INTERFACE,
    MARKDOWN_TEXTAREA_INTERFACE,
    NUMERIC_INTERFACE,
)
from workflow.report.json import (
    ChecklistDefinitionTrendsJsonReport,
    TemplateRunsJsonReport,
)


class PdfReportBase:
    """Base class for PDF report"""

    BASE_BOLD_FONT = tt2ps(canvas_basefontname, 1, 0)
    BASE_FONT = canvas_basefontname

    def _get_source_data(self):
        """
        Get source data to convert to PDF
        """
        raise NotImplementedError(
            "This method is expected to be implemented in the sub-class."
        )

    def write_pdf(self, response):
        """
        Write PDF to response
        """
        raise NotImplementedError(
            "This method is expected to be implemented in the sub-class."
        )

    @staticmethod
    def get_progress_bar_drawing(completed: int, total: int):
        """
        Draw progress bar for task

        Args:
            completed: int. Completed count
            total: int. Total count
        """
        drawing = Drawing(400, 20)
        width = 300
        ratio = 0
        if total > 0:
            ratio = completed / total
        percentage = ratio * 100
        progress_width = ratio * width
        full_bar = Rect(0, 0, width, 15, fillColor=colors.white)
        progress_bar = Rect(0, 0, progress_width, 15, fillColor=colors.black)
        drawing.add(full_bar)
        drawing.add(progress_bar)
        drawing.add(String(305, 5, f"{percentage:3.2f}%"))

        return drawing

    @staticmethod
    def get_style_sheet():
        """
        Get stylesheet declaring styles
        """
        base = getSampleStyleSheet()
        base.add(
            ParagraphStyle(
                name="Bold",
                fontName=PdfReportBase.BASE_BOLD_FONT,
                fontSize=10,
                leading=12,
            )
        )
        base.add(
            ParagraphStyle(
                name="Heading1Center", parent=base["Heading1"], alignment=TA_CENTER
            )
        )
        base.add(
            ParagraphStyle(
                name="LargeBoldCenter",
                fontName=PdfReportBase.BASE_BOLD_FONT,
                fontSize=12,
                leading=16,
                alignment=TA_CENTER,
            )
        )
        base.add(
            ParagraphStyle(
                name="NormalCenter",
                fontName=PdfReportBase.BASE_FONT,
                fontSize=10,
                leading=12,
                alignment=TA_CENTER,
            )
        )
        base.add(
            ParagraphStyle(
                name="NormalWithSpaceBefore",
                parent=base["Normal"],
                spaceBefore=5,
            )
        )
        return base

    @classmethod
    def get_report_title_style(cls):
        """
        Get report title style
        """
        style_sheet = cls.get_style_sheet()
        return style_sheet["Heading1Center"]

    @staticmethod
    def get_table_style(**kwargs):
        """
        Get stylesheet declaring styles for table
        """
        valign = kwargs.pop("valign", "TOP")
        return TableStyle(
            [
                ("INNERGRID", (0, 0), (-1, -1), 0.25, colors.black),
                ("BOX", (0, 0), (-1, -1), 0.25, colors.black),
                ("ALIGN", (0, 0), (-1, -1), "LEFT"),
                ("VALIGN", (0, 0), (-1, -1), valign),
            ]
        )

    @classmethod
    def get_progress_text(
        cls, interface_type: str, completed: int, total: int, **kwargs
    ):
        """
        Get progress text for task

        Args:
            interface_type: str. Type of interface
            completed: int. Completed count
            total: int. Total count
        """
        style_sheet = kwargs.pop("style_sheet", cls.get_style_sheet())
        action_text = "Answered"
        if interface_type == CHECKBOX_INTERFACE:
            action_text = "Checked"
        return Paragraph(
            f"{action_text} {completed} of {total} times", style_sheet["Normal"]
        )

    def get_report_overview(self, info: dict) -> dict:
        """
        Override to get overview content for report
        """
        style_sheet = self.get_style_sheet()
        heading = Paragraph("Report Overview", style_sheet["Heading2"])
        content_list: list = []
        checklist_name = info.get("name")
        team_names = info.get("team_names")
        start_date = info.get("start_date")
        end_date = info.get("end_date")
        tags = info.get("tags")
        if checklist_name:
            definition_text = Paragraph(
                f"Checklist: {checklist_name}", style_sheet["Normal"]
            )
            content_list.append(definition_text)
        if team_names:
            teams_names_text = ", ".join((team["name"] for team in team_names))
            team_text = Paragraph(f"Teams: {teams_names_text}", style_sheet["Normal"])
            content_list.append(team_text)
        if start_date:
            start_date_text = Paragraph(
                f"Start date: {start_date}",
                style_sheet["Normal"],
            )
            content_list.append(start_date_text)
        if end_date:
            end_date_text = Paragraph(
                f"End date: {end_date}",
                style_sheet["Normal"],
            )
            content_list.append(end_date_text)
        if tags:
            end_date_text = Paragraph(
                f"Tags: {', '.join(tags)}",
                style_sheet["Normal"],
            )
            content_list.append(end_date_text)
        return {
            "heading": heading,
            "subtitle": None,
            "content_list": content_list,
        }

    def get_report_title(self):
        """
        Get Report title
        """
        raise NotImplementedError(
            "This method is expected to be implemented in the sub-class."
        )


class TemplateRunsPdfReport(TemplateRunsJsonReport, PdfReportBase):
    """
    Generate Recurrence Run CSV report
    """

    def __init__(self, instance, *args, **kwargs):
        super().__init__(instance, *args, **kwargs)
        self.info = kwargs.get("info", {})

    def _get_source_data(self):
        """
        Get source data to convert to PDF
        """
        data = self.get_json_data()
        definition_id_map: dict = self.group_tasks_by_definition_id(data)
        return definition_id_map

    def get_report_title(self) -> Paragraph:
        """
        Get report title
        """
        title_style = self.get_report_title_style()
        return Paragraph("Checklist definition Report", title_style)

    @classmethod
    def get_tasks_table(cls, tasks: dict, dates: set) -> list:
        """
        Get task table

        Args:
            tasks: dict. dict of serialized assignee: tasks
        """
        headers = ["Team member"]
        headers.extend(list(dates))
        style_sheet = cls.get_style_sheet()
        table_data = [[Paragraph(item, style_sheet["Bold"]) for item in headers]]
        for assignee, responses in tasks.items():
            name = f"{responses['assignee_name']} @{assignee}"
            row = [Paragraph(name, style_sheet["Normal"])]
            for date_key in dates:
                value = Paragraph("No answer", style_sheet["Normal"])
                response = responses.get(date_key)
                if response:
                    value = Paragraph(responses[date_key], style_sheet["Normal"])
                row.append(value)
            _row = deepcopy(row)
            table_data.append(_row)
        return table_data

    # pylint: disable=too-many-arguments
    @classmethod
    def build_task_content(
        cls,
        tasks: dict,
        completed: int,
        total: int,
        interface_type: str,
        task_label: str,
        dates: set,
    ):
        """
        Build task PDF content including heading, subtitle and content_list

        Args:
            tasks: list. Tasks list
            completed: int. Completed count
            total: int. Total count
            interface_type: str
            task_label: str
            dates: set
        """
        style_sheet = cls.get_style_sheet()
        table_style = cls.get_table_style()
        heading = Paragraph(task_label, style_sheet["Heading3"])
        content_list = []
        subtitle = cls.get_progress_text(interface_type, completed, total)
        tasks_table = cls.get_tasks_table(tasks, dates)
        task_table = Table(tasks_table, spaceBefore=10, spaceAfter=10, hAlign="LEFT")
        task_table.setStyle(table_style)
        content_list.append(task_table)
        return {
            "heading": heading,
            "subtitle": subtitle,
            "content_list": content_list,
        }

    def write_pdf(self, response):
        """
        Write PDF to response
        """
        buffer = io.BytesIO()
        template = SimpleDocTemplate(buffer, pagesize=A4)
        style_sheet = self.get_style_sheet()
        page_elements = []
        definition_id_map = self._get_source_data()

        page_elements.append(self.get_report_title())

        overview_content = self.get_report_overview(self.info)
        page_elements.append(overview_content["heading"])
        page_elements += overview_content["content_list"]

        page_elements.append(Paragraph("Tasks", style_sheet["Heading2"]))
        for tasks in definition_id_map.values():
            task_section_content = self.build_task_content(**tasks)
            page_elements.append(task_section_content["heading"])
            page_elements.append(task_section_content["subtitle"])
            page_elements += task_section_content["content_list"]
        template.build(page_elements)
        response.write(buffer.getvalue())
        buffer.close()


class ChecklistDefinitionTrendsPdfReport(
    ChecklistDefinitionTrendsJsonReport, PdfReportBase
):
    """
    Generate Recurrence trend report
    """

    def __init__(self, instance, *args, **kwargs):
        super().__init__(instance, *args, **kwargs)
        self.info = kwargs.get("info", {})

    def _get_source_data(self):
        """
        Get source data to convert to PDF
        """
        return self.get_json_data()

    @classmethod
    def get_report_title(cls):
        """
        Get Report title
        """
        title_style = cls.get_report_title_style()
        return Paragraph("Task Trends Report", title_style)

    @classmethod
    def parse_response_rates(cls, response_rates: list[dict]) -> tuple[list, list]:
        """
        Parse response rates to a tuple of timestamps and percentages
        """
        timestamps: list = []
        percentages: list = []
        for response_rate in response_rates:
            completed = response_rate["completed_count"]
            total = response_rate["total_count"]
            percentage = 0
            if total > 0:
                percentage = float(completed / total * 100)
            timestamp_dt = parse_datetime(response_rate["timestamp"])
            percentages.append(percentage)
            timestamps.append(timestamp_dt)

        return timestamps, percentages

    @classmethod
    def get_response_rate_content(
        cls, timestamps: list[datetime], percentages: list[float]
    ) -> dict:
        """
        Get response rate content for PDF

        Args:
            timestamps: list[datetime]. List of timestamp datetime objects
            percentages: list[float]. List of response rates in percentage
        """
        style_sheet = cls.get_style_sheet()
        heading = Paragraph("Response Rates", style_sheet["Heading2"])
        axes = plt.figure().add_subplot(1, 1, 1)
        axes.set_ylim(0, 100)
        axes.yaxis.set_major_formatter(PercentFormatter())
        axes.xaxis.set_ticks(timestamps)
        axes.tick_params(axis="x", which="major", labelsize=5)
        axes.xaxis.set_major_formatter(mdates.DateFormatter("%b %d"))
        plt.plot(timestamps, percentages, linestyle="--", marker="o", color="black")
        imgdata = io.BytesIO()
        plt.savefig(imgdata, format="png")
        imgdata.seek(0)
        plt.close()
        return {
            "heading": heading,
            "subtitle": None,
            "content_list": [Image(imgdata, width=500, height=333, hAlign="LEFT")],
        }

    @classmethod
    def get_checkbox_trends_content(cls, task_trends_obj: dict):
        """
        Get trends content for checkbox task definition

        Args:
            trend: dict.
        """
        progress_bar = cls.get_progress_bar_drawing(
            task_trends_obj["response_rate"]["completed_count"],
            task_trends_obj["response_rate"]["total_count"],
        )
        return [progress_bar]

    @classmethod
    def get_markdown_trends_content(cls, task_trends_obj: dict):
        """
        Get trends content for markdown task definition

        Args:
            trend: dict.
        """
        progress_bar = cls.get_progress_bar_drawing(
            task_trends_obj["response_rate"]["completed_count"],
            task_trends_obj["response_rate"]["total_count"],
        )
        return [progress_bar]

    @classmethod
    def get_numeric_trends_content(cls, task_trends_obj: dict):
        """
        Get trends content for numeric task definition

        Args:
            task_trends_obj: dict.
        """
        content_list: list[Paragraph] = []
        style_sheet = cls.get_style_sheet()
        mean = 0
        median = 0
        mode = 0
        interface_stats = task_trends_obj["interface_stats"]
        if interface_stats:
            mean = interface_stats["mean"]
            median = interface_stats["median"]
            mode = interface_stats["mode"]
        mean_text = Paragraph(f"Mean: <b>{mean:.2f}</b>", style_sheet["Normal"])
        median_text = Paragraph(f"Median: <b>{median:.2f}</b>", style_sheet["Normal"])
        mode_text = Paragraph(f"Mode: <b>{mode:.2f}</b>", style_sheet["Normal"])
        content_list.append(mean_text)
        content_list.append(median_text)
        content_list.append(mode_text)
        return content_list

    @classmethod
    def get_linear_scale_values_percentages(
        cls, buckets, min_value, max_value, total
    ) -> tuple[list, list]:
        """
        Get linear scale rating values and counts
        """
        values = list(range(min_value, max_value + 1))
        percentages = [0] * len(values)
        for index, bucket in enumerate(buckets):
            percentages[index] = bucket["count"] / total * 100

        return values, percentages

    @classmethod
    def draw_linear_scale_chart(
        cls, values, percentages, min_value_description, max_value_description
    ) -> io.BytesIO:
        """
        Draw Linear Scale chart and return as BytesIO
        """
        imgdata = io.BytesIO()
        min_value = values[0]
        max_value = values[-1]
        axes = plt.figure(figsize=(10, 5)).add_subplot(1, 1, 1)
        for border in ["top", "right", "bottom", "left"]:
            axes.spines[border].set_visible(False)
        axes.get_yaxis().set_ticks([])
        axes.get_xaxis().set_ticks(list(range(min_value, max_value + 1)))
        full_bars = axes.bar(
            values, [100 for _ in values], color="white", edgecolor="black"
        )
        axes.bar(values, percentages, color="black")
        axes.set_ylim([0, 100])
        chart_width = -1
        for index, chart_bar in enumerate(full_bars):
            if chart_width < 0:
                chart_width = chart_bar.get_width()
            plt.text(
                chart_bar.get_x() + chart_bar.get_width() / 2,
                chart_bar.get_y() + chart_bar.get_height() * 1.01,
                f"{percentages[index]:3.2f}%",
                ha="center",
            )
        axes.text(
            min_value - chart_width / 2,
            -10,
            min_value_description,
            size=10,
            ha="right",
        )
        axes.text(
            max_value + chart_width / 2,
            -10,
            max_value_description,
            size=10,
            ha="left",
        )
        plt.savefig(imgdata, format="png")
        imgdata.seek(0)
        plt.close()
        return imgdata

    @classmethod
    def get_linear_scale_chart_image(
        cls, interface_stats: dict, customization_args: dict, total: int
    ) -> Image:
        """
        Get image for Linear Scale Rating data chart
        """
        if total <= 0:
            return None
        values, percentages = cls.get_linear_scale_values_percentages(
            interface_stats["buckets"],
            customization_args["min_value"],
            customization_args["max_value"],
            total,
        )
        imgdata = cls.draw_linear_scale_chart(
            values,
            percentages,
            customization_args["min_value_description"],
            customization_args["max_value_description"],
        )
        return Image(imgdata, width=500, height=250, hAlign="CENTER")

    @classmethod
    def get_linear_scale_trends_content(cls, task_trends_obj: dict):
        """
        Get trends content for linear scale rating task definition

        Args:
            task_definition: dict.
            completed: int.
            total: int.
            interface_stats: dict. Statistic data of tasks.
        """
        style_sheet = cls.get_style_sheet()
        mean = 0
        median = 0
        mode = 0
        content_list = []
        interface_stats = task_trends_obj["interface_stats"]

        if interface_stats:
            mean = interface_stats["mean"]
            median = interface_stats["median"]
            mode = interface_stats["mode"]

        bucket_chart_image = cls.get_linear_scale_chart_image(
            interface_stats,
            task_trends_obj["task_definition"]["customization_args"],
            task_trends_obj["response_rate"]["completed_count"],
        )

        content_list.append(bucket_chart_image)
        content_list.append(
            Paragraph(f"Mean: <b>{mean:.2f}</b>", style_sheet["NormalWithSpaceBefore"])
        )
        content_list.append(
            Paragraph(f"Median: <b>{median:.2f}</b>", style_sheet["Normal"])
        )
        content_list.append(Paragraph(f"Mode: <b>{mode}</b>", style_sheet["Normal"]))
        return content_list

    @classmethod
    def build_task_trends_content(cls, task_trends_obj: dict):
        """
        Build task trends content for different interface type
        """
        task_definition = task_trends_obj["task_definition"]
        response_rate = task_trends_obj["response_rate"]
        style_sheet = cls.get_style_sheet()
        heading = Paragraph(task_definition["label"], style_sheet["Heading3"])
        subtitle = cls.get_progress_text(
            task_definition["interface_type"],
            response_rate["completed_count"],
            response_rate["total_count"],
        )
        trends_content: list = []
        if task_definition["interface_type"] == LINEAR_SCALE_RATING_INTERFACE:
            trends_content = cls.get_linear_scale_trends_content(task_trends_obj)
        if task_definition["interface_type"] == NUMERIC_INTERFACE:
            trends_content += cls.get_numeric_trends_content(task_trends_obj)
        elif task_definition["interface_type"] == CHECKBOX_INTERFACE:
            trends_content += cls.get_checkbox_trends_content(task_trends_obj)
        elif task_definition["interface_type"] == MARKDOWN_TEXTAREA_INTERFACE:
            trends_content += cls.get_markdown_trends_content(task_trends_obj)

        return {
            "heading": heading,
            "subtitle": subtitle,
            "content_list": trends_content,
        }

    def write_pdf(self, response):
        """
        Write PDF to response

        Args:
            response: HttpResponse. Response to write PDF data to
        """
        buffer = io.BytesIO()
        template = SimpleDocTemplate(buffer, pagesize=A4)
        page_elements = []
        data = self._get_source_data()
        style_sheet = self.get_style_sheet()

        page_elements.append(self.get_report_title())

        overview_content = self.get_report_overview(self.info)
        page_elements.append(overview_content["heading"])
        page_elements += overview_content["content_list"]

        timestamps, response_rates = self.parse_response_rates(data["response_rates"])
        response_rate_content = self.get_response_rate_content(
            timestamps, response_rates
        )
        page_elements.append(response_rate_content["heading"])
        page_elements.append(response_rate_content["subtitle"])
        for content in response_rate_content["content_list"]:
            page_elements.append(content)

        task_trends_heading = Paragraph("Tasks Trends", style_sheet["Heading2"])
        page_elements.append(task_trends_heading)
        for task_trends_obj in data["trends"]:
            task_trends_content = self.build_task_trends_content(task_trends_obj)
            page_elements.append(task_trends_content["heading"])
            page_elements.append(task_trends_content["subtitle"])
            page_elements += task_trends_content["content_list"]
        template.build(page_elements)
        response.write(buffer.getvalue())
        buffer.close()
