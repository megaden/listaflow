"""
Combine multiple trend stats
"""
import statistics
from collections import Counter
from dataclasses import dataclass, field
from typing import List


@dataclass
class TaskResponseRate:
    """
    build response rate trends by accumulating total and completed task for given runs
    """

    completed_count: int = 0
    total_count: int = 0

    def register(self, completed_count, total_count):
        """
        register single response rate
        """
        self.completed_count += completed_count
        self.total_count += total_count


@dataclass
class NumericTrendBuilder:
    """
    build numeric trends by accumulation numeric interface stats for given runs
    """

    numeric_value_key: str
    mean: float = 0
    mode: float = 0
    median: float = 0
    count: int = 0
    buckets: List[dict] = field(default_factory=list)

    def combined_median_mode(self):
        """
        get median and mode for existing + new stats
        """
        nums = [[item[self.numeric_value_key]] * item["count"] for item in self.buckets]
        flat_list = sorted([num for num_list in nums for num in num_list])
        if flat_list:
            return (
                statistics.median(flat_list),
                statistics.mode(flat_list),
                statistics.mean(flat_list),
            )
        return 0, 0, 0

    def combine_buckets(self, buckets, users):
        """
        Combine buckets with self
        """
        buckets_counter = Counter(
            {
                item[self.numeric_value_key]: len(
                    set(item.get("users", [])).intersection(users)
                )
                for item in buckets
            }
        )
        new_buckets_counter = Counter(
            {item[self.numeric_value_key]: item["count"] for item in self.buckets}
        )
        buckets_counter.update(new_buckets_counter)
        return sorted(
            [
                {"count": val, self.numeric_value_key: key}
                for key, val in buckets_counter.items()
            ],
            key=lambda x: x[self.numeric_value_key],
        )

    def register(self, interface_stats, count, users):
        """
        register single interface_stats
        """
        if not interface_stats:
            return
        self.count = self.count + count
        self.buckets = self.combine_buckets(
            interface_stats.get("buckets", []), set(users)
        )
        self.median, self.mode, self.mean = self.combined_median_mode()
