"""Celery tasks for the app."""

from typing import Optional
from celery import shared_task
from django.core import mail
from django.db import transaction
from django.utils import timezone

from utils.cache import clear_cache, clear_cache_multi
from workflow.helpers.email import send_pending_checklist_email_for_runs
from workflow.models import (
    ChecklistTaskDefinition,
    Recurrence,
    Run,
    RunTrendTaskReport,
)


@shared_task(name="workflow.tasks.create_checklist_for_users")
def create_checklist_for_users(recurrence_id):
    """Task to create checklists and send emails to the users.

    Args:
        recurrence_id: str. The Id of the Recurrence model.
    """
    recurrence_instance = Recurrence.objects.get(id=recurrence_id)
    with transaction.atomic():
        Run.create_run_for_team(
            recurrence_instance.team,
            recurrence_instance.checklist_definition,
            recurrence_instance,
        )


@shared_task
def compute_task_definition_trends_report(task_definition_id: str, run_id: str = None):
    """Task to compute report every time a ChecklistTask is saved

    Args:
        task_definition_id: str. The ID of the TaskDefinition instance.
        recurrence_id: str. Recurrence ID, compute all recurrences if not passed
    """
    task_definition = ChecklistTaskDefinition.objects.get(id=task_definition_id)
    run = Run.objects.filter(id=run_id).first()
    with transaction.atomic():
        RunTrendTaskReport.compute_task_trends(task_definition, run)


@shared_task
def flush_cache(cache_key: str):
    """
    Task to clear cache for given cache key
    """
    clear_cache(cache_key)


@shared_task(name="workflow.tasks.send_reminder_emails")
def send_reminder_emails(today_str: Optional[str] = None):
    """
    Task to send reminder mail to each assignee.
    """

    today = timezone.now().date()
    if today_str:
        today = timezone.datetime.strptime(today_str, "%Y-%m-%d").date()

    def _should_send_mail(due_on, reminders):
        for reminder_days in reminders:
            reminder_date = due_on - timezone.timedelta(days=reminder_days)
            if reminder_date == today:
                return True
        return False

    valid_runs = Run.objects.filter(due_date__gt=today, recurrence__isnull=False)
    with mail.get_connection() as conn:
        for valid_run in valid_runs:
            due_date = valid_run.due_date
            reminders = valid_run.reminders
            if _should_send_mail(due_date, reminders):
                days_pending = due_date - today
                send_pending_checklist_email_for_runs(valid_run, conn, days_pending)


@shared_task
def flush_many_cache(cache_key_regex: str):
    """
    Clears multiple cache for given regex.
    """
    clear_cache_multi(cache_key_regex)
