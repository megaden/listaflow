"""Workflow Enum classes"""
from utils.type import DjangoChoiceEnum


class ChecklistListName(DjangoChoiceEnum):
    """
    One tab corresponding to one queryset for listing view
    """

    ALL = "all"
    TO_DO = "to_do"
    ASSIGNED_TO_ME = "assigned_to_me"


class ChecklistStatus(DjangoChoiceEnum):
    """Enumeration of all checklists statuses"""

    UP_COMING = "Up coming"
    TO_DO = "To do"
    IN_PROGRESS = "In progress"
    PAST_DUE = "Past due"
    COMPLETED = "Completed"


class ChecklistArchiveAction(DjangoChoiceEnum):
    """Enumeration of all archive related actions"""

    ARCHIVE = "Archive"
    UNARCHIVE = "Unarchive"
