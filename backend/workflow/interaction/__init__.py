"""Pulic constants and methods for interaction module."""

from workflow.interaction.interfaces import (
    CheckboxInterface,
    LinearScaleRatingInterface,
    MarkdownTextAreaInterface,
    MultiChoiceInterface,
    NumericInterface,
)

MARKDOWN_TEXTAREA_INTERFACE = "markdown_textarea"
LINEAR_SCALE_RATING_INTERFACE = "linear_scale_rating"
CHECKBOX_INTERFACE = "checkbox"
NUMERIC_INTERFACE = "numeric"
MULTIPLE_CHOICE_INTERFACE = "multi_choice"

INTERFACE_TYPE_CHOICES = [
    (CHECKBOX_INTERFACE, "Checkbox"),
    (MARKDOWN_TEXTAREA_INTERFACE, "Markdown textarea"),
    (LINEAR_SCALE_RATING_INTERFACE, "Linear scale rating"),
    (NUMERIC_INTERFACE, "Numeric"),
    (MULTIPLE_CHOICE_INTERFACE, "Multiple choice"),
]

INTERFACE_TYPE_TO_CLASS = {
    CHECKBOX_INTERFACE: CheckboxInterface,
    MARKDOWN_TEXTAREA_INTERFACE: MarkdownTextAreaInterface,
    LINEAR_SCALE_RATING_INTERFACE: LinearScaleRatingInterface,
    NUMERIC_INTERFACE: NumericInterface,
    MULTIPLE_CHOICE_INTERFACE: MultiChoiceInterface,
}


def get_interfaces_customization_arg_schema():
    """Generates schema for all the interface's customization args.

    Returns:
        dict(str, str): A dict with interface name as key and json-schema as
        value.
    """
    schema = {}
    for interface_type, klass in INTERFACE_TYPE_TO_CLASS.items():
        schema[interface_type] = klass.get_customization_args_schema()

    return schema


def get_interfaces_customization_arg_default_value():
    """Generates default value for all the interface's customization args.

    Returns:
        dict(str, dict): A dict with interface name as key and default value
        of corresponding interface as value.
    """
    interface_name_to_default_value = {}
    for interface_name, klass in INTERFACE_TYPE_TO_CLASS.items():
        interface_name_to_default_value[
            interface_name
        ] = klass.get_customization_args_default_value()

    return interface_name_to_default_value


def response_repr(response, interface_type, customization_args):
    """
    Get string representation of a response
    """
    interface_cls = INTERFACE_TYPE_TO_CLASS[interface_type]
    return interface_cls.to_str(response, customization_args)
