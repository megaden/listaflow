"""Listaflow URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path

from rest_framework_nested import routers
from rest_framework_bulk import routes as bulk_routers

from workflow.views import (
    ChecklistDefinitionListViewSet,
    ChecklistDefinitionViewSet,
    ChecklistTaskDefinitionViewSet,
    RecurrenceViewSet,
    RecurrenceRunViewSet,
    UserChecklistViewSet,
    ChecklistViewSet,
    ChecklistTaskViewSet,
)

# The routing here can get a little confusing, but to automate much of it and make
# sure all needed routes are included, we're using drf-nested routers.
# There's a good bit of automation behind this and names can get a little magic
# but it should make maintenance much easier.
#
# One curiosity I've not yet resolved is why the 'basename' constructs useful
# names in swagger, but a different method is used to construct the names for
# 'reverse' calls. It would be nice to have them be consistent. See the view
# tests for examples of how these names are actually constructed.
# https://github.com/alanjds/drf-nested-routers

checklist_definition_router = routers.SimpleRouter()
checklist_definition_router.register(r"definition", ChecklistDefinitionViewSet)
checklist_definition_router.register(
    r"short-definition", ChecklistDefinitionListViewSet, basename="short-definition"
)

checklist_definition_task_definition_router = routers.NestedSimpleRouter(
    checklist_definition_router,
    "definition",
    lookup="checklist_definition",
)
checklist_definition_task_definition_router.register(
    "tasks",
    ChecklistTaskDefinitionViewSet,
    basename="checklist-definition-task-definitions",
)


checklist_router = bulk_routers.BulkRouter()
checklist_router.register(r"list", ChecklistViewSet, basename="checklists")

user_checklist_router = routers.SimpleRouter()
user_checklist_router.register(
    r"checklist", UserChecklistViewSet, basename="user-checklists"
)

user_checklist_task_router = routers.NestedSimpleRouter(
    user_checklist_router,
    "checklist",
    lookup="checklist",
)
user_checklist_task_router.register(
    "task", ChecklistTaskViewSet, basename="checklist-tasks"
)


checklist_recurrence_router = routers.SimpleRouter()
checklist_recurrence_router.register(
    r"recurrence",
    RecurrenceViewSet,
    basename="checklist-recurrence",
)

checklist_recurrence_run_router = routers.NestedSimpleRouter(
    checklist_recurrence_router,
    "recurrence",
    lookup="recurrence",
)

checklist_recurrence_run_router.register(
    "run",
    RecurrenceRunViewSet,
    basename="checklist-recurrence-run",
)

app_name = "workflow"

urlpatterns = [
    path("checklist/", include(checklist_router.urls)),
    path("checklist/", include(checklist_definition_router.urls)),
    path("checklist/", include(checklist_definition_task_definition_router.urls)),
    path("checklist/", include(checklist_recurrence_router.urls)),
    path("checklist/", include(checklist_recurrence_run_router.urls)),
    path("user/<username>/", include(user_checklist_router.urls)),
    path("user/<username>/", include(user_checklist_task_router.urls)),
]
