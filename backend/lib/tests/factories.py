"""
Test model factories for the lib app.
"""
from factory import Faker
from factory.django import DjangoModelFactory

from lib.models import Tag


class TagFactory(DjangoModelFactory):
    """
    Factory for Tag model.
    """

    name = Faker("name")

    class Meta:
        model = Tag
