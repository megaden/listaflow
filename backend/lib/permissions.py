"""
Permission classes that could be useful anywhere.
"""
from typing import Any

from rest_framework.permissions import BasePermission
from rest_framework.request import Request
from rest_framework.views import APIView


def is_safe(request: Request) -> bool:
    """
    Checks if a request method is safe.
    """
    return request.method in ["GET", "HEAD", "OPTIONS"]


class IsSafeMethod(BasePermission):
    """
    Checks if a request is using a 'safe' method according to HTTP convention.
    """

    message = "You are not permitted to modify this."

    def has_permission(self, request: Request, view: APIView) -> bool:
        """
        View-wide check.
        """
        return is_safe(request)

    def has_object_permission(self, request: Request, view: APIView, obj: Any) -> bool:
        """
        Makes sure this check isn't skipped when applying to a specific object.
        """
        return is_safe(request)
