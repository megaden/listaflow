Hey, {{ display_name }}! 👋

It’s time to complete {{ checklist_name }} for {{ run_start_month_name }} {{ run_start_day_of_month }} to {{ run_end_month_name }} {{ run_end_day_of_month }}. Please add your update before the end of day this {{ due_date_day_of_week }} ({{ due_date_month_name }} {{ due_date_day_of_month }}).

Here's a sneak peek of the items awaiting your response:
{% for item in task_list %}
* {{ item }}
{% endfor %}
Link: {{ site_url }}/lists/{{ checklist_id }}
