# Internationalization

**Backend - Translation**

1. Use [gettext functions provided by Django](https://docs.djangoproject.com/en/3.2/topics/i18n/translation/) to refer to a translated text's key. Note: you can use a comment with `Translators:` as prefix to add notes for translators, this is also referred in the translation document by Django.
2. Run the following command to create new corresponding keys for all languages
```sh
make makemessages.backend
```
3. Fill translated texts for created message
4. Run compile command to generate `*.mo` files from `*.po` files.
```sh
make compilemessages.backend
```
5. Commit changed `*.po` and `*.mo` files.

Translation files for backend are located at: [./backend/locale](./backend/locale/)

Notes: When adding new language, it's worth noting that `make messages.backend` can't create the initial translation file. You need to:
- Run `make shell`
- Run `python manage.py makemessages -l <new_language_code>`

This procedure applies to add a language for the first time.

**Frontend - Translation**

Workflow for developer when adding a new text to the web app
1. Use supported methods from [`react-i18next` library](https://react.i18next.com/) to refer to text using a key
2. Add text's key and text to primary language locale file (e.g: For English, it is located at [`./frontend/public/static/locales/en/translation.json`](./frontend/public/static/locales/en/translation.json))
3. Run below command for syncing translation files of secondary languages. List of secondary languages is referred in [configuration file for syncing](./frontend/locales.sync.config.js).
```sh
make makemessages.frontend
```
4. Translate other languages using synced files if it's possible
5. Commit all changed JSON translation files

Translation files for frontend are located at: [./frontend/public/static/locales/](./frontend/public/static/locales/)

**Frontend - Working with RTL interface**

- When embedding user-generated content that possibly has mixed left-to-right (LTR) and right-to-left (RTL) content, make sure to use [`bdi`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/bdi) tag to wrap it. This HTML tag is also helpful in case you want to keep direction of a text put in a mixed content (e.g: date string).
- Add `dir=auto` attribute to text input elements to allow input of bi-directional content.
- We use [react-markdown](https://www.npmjs.com/package/react-markdown) for displaying and inputting markdown content. If it's a must to add wrapper to renderred elements, override the component in [ReactMarkdownOverrides.tsx](./src/components/ReactMarkdownOverrides.tsx) and make sure these overrides passed to ReactMarkdown using [components](https://github.com/remarkjs/react-markdown#appendix-b-components) attribute.

```jsx
import ReactMarkdownOverrides from "./ReactMarkdownOverrides";
...
  return (
    <ReactMarkdown
      children={text}
      remarkPlugins={[remarkGfm]}
      components={ReactMarkdownOverrides}
    />
  );
...
```
