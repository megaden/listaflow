# Developing Listaflow

In order to develop listaflow:

We need docker and docker-compose installed on the machine.

```
Clone the repo with ``$ git clone git@gitlab.com:opencraft/dev/listaflow.git``

$ cd listaflow

$ make
```

`make` does the initial scaffolding set-up.

There will be a need to create superuserm, the way to do this is `$ make createsuperuser`
this will create the super user with:

```
username: admin
email: admin@example.com
password: admin

```
The logs can be tailed through:
```
$ make logs
```

The quality checks/tests can be run with:

```
$ make qa
```

Or, just run tests with:

```
$ make test
```

...Or just quality checks with:

```
$ make quality
```

You can also watch the logs in real time with:

```
$ make run
```
